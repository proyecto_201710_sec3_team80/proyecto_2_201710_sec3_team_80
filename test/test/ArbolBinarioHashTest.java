package test;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.junit.Test;

import model.data_structures.ArbolBinarioHash;
import model.data_structures.BinaryTreeHashTable;
import model.data_structures.BinaryTreeHashTableTags;
import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;
import model.vo.VOPeliculaUsuarios;
import model.vo.VOUsuario;

public class ArbolBinarioHashTest {

	private ArbolBinarioHash<Date, VOPeliculaUsuarios> tree;
	private DateFormat format= new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);

	public void setupEscenario1(){
		tree = new ArbolBinarioHash<Date, VOPeliculaUsuarios>();
	}

	public void setupEscenario2(){

		tree = new ArbolBinarioHash<Date, VOPeliculaUsuarios>();

		VOPeliculaUsuarios peliculaUsuario1 = new VOPeliculaUsuarios();


		String[] actores = new String[4]; 
		actores[0] = "Carlos";
		actores[1] = "Luis";
		actores[2] = "Mat Daemon";
		actores[3] = "Leonardo Dicaprio";

		ListaEncadenada<String> generos = new ListaEncadenada<String>();

		generos.agregarElementoFinal("Animation");
		//Se crea el VOPelicula y se le asignan todos los datos
		VOPelicula peli1 = new VOPelicula();
		peli1.setActores(actores);

		peli1.setDirector("Machete");
		peli1.setIdPelicula(1);
		peli1.setNumeroRatings(9);
		peli1.setNumeroTags(4);
		peli1.setPromedioRatings(4.5);
		peli1.setRatingIMDB(6);
		peli1.setGenerosAsociados(generos);

		ListaEncadenada<String> tags = new ListaEncadenada<String>();

		tags.agregarElementoFinal("Buena peli");
		tags.agregarElementoFinal("Excelente");
		tags.agregarElementoFinal("De lo mejor");
		tags.agregarElementoFinal("Recomendada");

		peli1.setTagsAsociados(tags);
		peli1.setTitulo("Toy Sory");

		BinaryTreeHashTableTags<String, VOUsuario> tablaUsuarios = new BinaryTreeHashTableTags<String, VOUsuario>(4);

		VOUsuario user1 = new VOUsuario();
		user1.setDiferenciaOpinion(12);
		user1.setIdUsuario(1259);
		user1.setNumRatings(12);
		user1.setPrimerTimestamp(1998);
		user1.setTag("Buena peli");
		tablaUsuarios.put(user1.getTag(), user1);
		
		Date fecha = null;
		try {
		 fecha = format.parse("2 May 2000");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		peli1.setFechaLanzamiento(fecha);

		peliculaUsuario1.setPelicula(peli1);
		peliculaUsuario1.setUsuarios(tablaUsuarios);

		tree.put(peliculaUsuario1.getPelicula().getFechaLanzamiento(), peliculaUsuario1);

	}

	public void setupEscenario3(){

		tree = new ArbolBinarioHash<Date, VOPeliculaUsuarios>();
	
		

		VOPeliculaUsuarios peliculaUsuario1 = new VOPeliculaUsuarios();
		VOPeliculaUsuarios peliculaUsuario2 = new VOPeliculaUsuarios();
		VOPeliculaUsuarios peliculaUsuario3 = new VOPeliculaUsuarios();
		
		Date fecha = null;
		try {
		 fecha = format.parse("2 May 2000");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		//Actores de la peli1
		String[] actores1 = new String[5]; 
		actores1[0] = "Line";
		actores1[1] = "Walt";
		actores1[2] = "Cameron";
		actores1[3] = "Leonardo";
		actores1[4] = "Ana";
		
		ListaEncadenada<String> generos = new ListaEncadenada<String>();

		generos.agregarElementoFinal("Action");
		//Se crea el VOPelicula y se le asignan todos los datos
		VOPelicula peli1 = new VOPelicula();

		peli1.setFechaLanzamiento(fecha);
		peli1.setActores(actores1);
		
		peli1.setDirector("Vin diesel");
		peli1.setGenerosAsociados(generos);
		peli1.setIdPelicula(6);
		peli1.setNumeroRatings(5);
		peli1.setNumeroTags(3);
		peli1.setPromedioRatings(4);
		peli1.setRatingIMDB(9);

		//Tags Para la peli1
		ListaEncadenada<String> tags = new ListaEncadenada<String>();

		tags.agregarElementoFinal("Buena peli");
		tags.agregarElementoFinal("Excelente");
		tags.agregarElementoFinal("De lo mejor");

		peli1.setTagsAsociados(tags);
		peli1.setTitulo("Rain");

		BinaryTreeHashTableTags<String, VOUsuario> tablaUsuarios = new BinaryTreeHashTableTags<String, VOUsuario>(4);

		VOUsuario user1Peli1 = new VOUsuario();

		user1Peli1.setDiferenciaOpinion(12);
		user1Peli1.setIdUsuario(1259);
		user1Peli1.setNumRatings(12);
		user1Peli1.setPrimerTimestamp(1998);
		user1Peli1.setTag("Buena peli");
		tablaUsuarios.put(user1Peli1.getTag(), user1Peli1);

		VOUsuario user2Peli1 = new VOUsuario();

		user2Peli1.setDiferenciaOpinion(-1);
		user2Peli1.setIdUsuario(2031);
		user2Peli1.setNumRatings(2);
		user2Peli1.setPrimerTimestamp(2000);
		user2Peli1.setTag("Excelente");
		tablaUsuarios.put(user2Peli1.getTag(), user2Peli1);

		VOUsuario user3Peli1 = new VOUsuario();

		user3Peli1.setIdUsuario(78964);
		user3Peli1.setTag("De lo mejor");
		tablaUsuarios.put(user3Peli1.getTag(), user3Peli1);

		VOUsuario user4Peli1 = new VOUsuario();

		user4Peli1.setIdUsuario(16);
		user4Peli1.setTag("Recomendada");
		tablaUsuarios.put(user4Peli1.getTag(), user4Peli1);

		peliculaUsuario1.setPelicula(peli1);
		peliculaUsuario1.setUsuarios(tablaUsuarios);

		//Se crea la segunda pelicula que ir� en la estructura
		VOPelicula peli2 = new VOPelicula();

		
		try {
		 fecha = format.parse("8 Jul 2052");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		peli2.setFechaLanzamiento(fecha);
		//Actores de la peli2
		String[] actores2 = new String[4]; 
		actores2[0] = "Albert einstein";
		actores2[1] = "Isaac Newton";
		actores2[2] = "Michael Faraday";
		actores2[3] = "Ana Reina";
		
		generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("Drama");

		peli2.setActores(actores2);
		peli2.setDirector("Marck Zuckemberg");
		peli2.setIdPelicula(31416);
		peli2.setNumeroRatings(10);
		peli2.setNumeroTags(10);
		peli2.setPromedioRatings(5);
		peli2.setRatingIMDB(10);
		peli2.setGenerosAsociados(generos);
		peli2.setFechaLanzamiento(fecha);

		//Tags de la peli2
		tags = new ListaEncadenada<String>();

		tags.agregarElementoFinal("Buena peli");
		tags.agregarElementoFinal("Excelente");
		tags.agregarElementoFinal("De lo mejor");
		tags.agregarElementoFinal("La vi 5 veces");

		peli2.setTagsAsociados(tags);
		peli2.setTitulo("Clone");

		//Tabla de usuarios que han dado rating a la peli2
		tablaUsuarios = new BinaryTreeHashTableTags<String, VOUsuario>(5);

		VOUsuario user1Peli2 = new VOUsuario();

		user1Peli2.setIdUsuario(1259);
		user1Peli2.setTag("Buena peli");
		tablaUsuarios.put(user1Peli2.getTag(), user1Peli2);

		VOUsuario user2Peli2 = new VOUsuario();



		user2Peli2.setIdUsuario(2031);
		user2Peli2.setTag("Excelente");
		tablaUsuarios.put(user2Peli2.getTag(), user2Peli2);

		VOUsuario user3Peli2 = new VOUsuario();

		user3Peli2.setIdUsuario(78964);
		user3Peli2.setTag("De lo mejor");
		tablaUsuarios.put(user3Peli2.getTag(), user3Peli2);

		VOUsuario user4Peli2 = new VOUsuario();

		user4Peli2.setIdUsuario(16);
		user4Peli2.setTag("La vi 5 veces");
		tablaUsuarios.put(user4Peli2.getTag(), user4Peli2);

		VOUsuario user5Peli2 = new VOUsuario();
		user5Peli2.setIdUsuario(9864);
		user5Peli2.setTag("Clone");
		tablaUsuarios.put(user5Peli2.getTag(), user5Peli2);

		//Se inserta la pelicula y la 
		peliculaUsuario2.setPelicula(peli2);
		peliculaUsuario2.setUsuarios(tablaUsuarios);


		//Se crea la tercera pelicula que ir� en la estructura
		VOPelicula peli3 = new VOPelicula();

		//Actores de la peli3
		String[] actores3 = new String[6]; 
		actores3[0] = "einstein";
		actores3[1] = "Newton";
		actores3[2] = "Faraday";
		actores3[3] = "Reina";
		actores3[4] = "Bay";
		actores3[5] = "Hitler";
		
		
		try {
			 fecha = format.parse("3 Dec 1997");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		peli3.setFechaLanzamiento(fecha);

		generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("Tragicomedia");
		peli3.setGenerosAsociados(generos);
		
		peli3.setActores(actores3);
		peli3.setDirector("Socrates");
		peli3.setIdPelicula(98566621);
		peli3.setNumeroRatings(2);
		peli3.setNumeroTags(2);
		peli3.setPromedioRatings(2);
		peli3.setRatingIMDB(2);


		//Tags de la peli3
		tags = new ListaEncadenada<String>();

		tags.agregarElementoFinal("Buena peli");
		tags.agregarElementoFinal("Excelente");
		tags.agregarElementoFinal("De lo mejor");
		tags.agregarElementoFinal("La vi 5 veces");
		tags.agregarElementoFinal("Los peores actores, no hacen bien su papel");
		tags.agregarElementoFinal("ahnuueunsuemasdf");

		peli2.setTagsAsociados(tags);
		peli2.setTitulo("Transformer 10");

		//Tabla de usuarios que han dado rating a la peli3
		tablaUsuarios = new BinaryTreeHashTableTags<String, VOUsuario>(5);

		VOUsuario user1Peli3 = new VOUsuario();

		user1Peli3.setIdUsuario(1259);
		user1Peli3.setTag("Buena peli");
		tablaUsuarios.put(user1Peli3.getTag(), user1Peli3);

		VOUsuario user2Peli3 = new VOUsuario();



		user2Peli3.setIdUsuario(2031);
		user2Peli3.setTag("Excelente");
		tablaUsuarios.put(user2Peli3.getTag(), user2Peli3);

		VOUsuario user3Peli3 = new VOUsuario();

		user3Peli3.setIdUsuario(78964);
		user3Peli3.setTag("De lo mejor");
		tablaUsuarios.put(user3Peli3.getTag(), user3Peli3);

		VOUsuario user4Peli3 = new VOUsuario();

		user4Peli3.setIdUsuario(16);
		user4Peli3.setTag("La vi 5 veces");
		tablaUsuarios.put(user4Peli3.getTag(), user4Peli3);

		VOUsuario user5Peli3 = new VOUsuario();
		user5Peli3.setIdUsuario(9864);
		user5Peli3.setTag("Clone");
		tablaUsuarios.put(user5Peli3.getTag(), user5Peli3);

		//Se inserta la pelicula y la tabla de usuarios
		peliculaUsuario3.setPelicula(peli3);
		peliculaUsuario3.setUsuarios(tablaUsuarios);

		tree.put(peliculaUsuario1.getPelicula().getFechaLanzamiento(), peliculaUsuario1);
		tree.put(peliculaUsuario2.getPelicula().getFechaLanzamiento(), peliculaUsuario2);
		tree.put(peliculaUsuario3.getPelicula().getFechaLanzamiento(), peliculaUsuario3);
	}

	@Test
	public void getTest(){
		
		setupEscenario1();
		
		Date fecha = null;
		
		try {
			 fecha = format.parse("2 May 2000");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		
		assertNull("No deber�a retornar un elemento", tree.get(fecha));



		setupEscenario2();

		assertNotNull("Deber�a contener un nodo con la fecha 2 May 2000: 1995", tree.get(fecha));


		setupEscenario3();

		
		try {
			 fecha = format.parse("2 May 2000");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		
		Date fecha1 = null;
		try {
			fecha1 = format.parse("8 Jul 2052");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		
		 Date fecha3 = null;
		try {
			 fecha3 = format.parse("3 Dec 1997");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		
		assertNotNull("Deber�a contener un nodo con el a�o: 2016", tree.get(fecha));
		assertNotNull("Deber�a contener un nodo con el a�o: 1997", tree.get(fecha1));
		assertNotNull("Deber�a contener un nodo con el a�o: 2020", tree.get(fecha3));
		
		VOPeliculaUsuarios peliPrueba = new VOPeliculaUsuarios();

		VOPelicula peli1 = new VOPelicula();

		try {
			 fecha3 = format.parse("9 Jun 3012");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		
		peli1.setFechaLanzamiento(fecha3);
		ListaEncadenada<String> generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("Terror");
		peli1.setGenerosAsociados(generos);
		peli1.setRatingIMDB(10);
		peli1.setTitulo("Cars 3");

		BinaryTreeHashTableTags<String, VOUsuario> arbolUsuarios = new BinaryTreeHashTableTags<String, VOUsuario>(2);
		VOUsuario user1 = new VOUsuario();
		user1.setTag("Buena");
		user1.setIdUsuario(125);
		VOUsuario user2 = new VOUsuario();
		user2.setTag("Excelente");
		user2.setIdUsuario(1896);

		arbolUsuarios.put(user1.getTag(), user1);
		arbolUsuarios.put(user2.getTag(), user2);

		peliPrueba.setPelicula(peli1);
		peliPrueba.setUsuarios(arbolUsuarios);

		tree.put(peliPrueba.getPelicula().getFechaLanzamiento(), peliPrueba);


		tree.inOrder();
	}

	@Test
	public void putTest(){
		setupEscenario1();
		Date fecha = null;
		try {
			 fecha = format.parse("9 Jun 3012");
			} catch (ParseException e) {
				e.printStackTrace();
			}

		assertNull("No deber�a retornar un elemento", tree.get(fecha));
		VOPeliculaUsuarios peliPrueba = new VOPeliculaUsuarios();

		VOPelicula peli1 = new VOPelicula();

		
		
		peli1.setFechaLanzamiento(fecha);
		ListaEncadenada<String> generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("Terror");
		peli1.setGenerosAsociados(generos);
		peli1.setRatingIMDB(10);
		peli1.setTitulo("Cars 3");

		BinaryTreeHashTableTags<String, VOUsuario> arbolUsuarios = new BinaryTreeHashTableTags<String, VOUsuario>(5);
		VOUsuario user1 = new VOUsuario();
		user1.setTag("Pesima");
		user1.setIdUsuario(125);
		VOUsuario user2 = new VOUsuario();
		user2.setTag("No hay comparaci�n");
		user2.setIdUsuario(1896);

		arbolUsuarios.put(user1.getTag(), user1);
		arbolUsuarios.put(user2.getTag(), user2);

		peliPrueba.setPelicula(peli1);
		peliPrueba.setUsuarios(arbolUsuarios);

		tree.put(peliPrueba.getPelicula().getFechaLanzamiento(), peliPrueba);
		assertNotNull("Deber�a existir un elemento con la llave: 3012", tree.get(fecha));

	}

	public void deleteTest(){
		setupEscenario3();
		
		Date fecha = null;
		try {
			 fecha = format.parse("3 Dic 1997");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		
		assertNotNull("Deber�a existir un nodo con la llave = 2020", tree.get(fecha));
		tree.delete(fecha);
		assertNull("No deber�a existir un nodo con la llave = 2020", tree.get(fecha));
	}

}
