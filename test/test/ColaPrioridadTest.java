package test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;
import model.data_structures.ColaPrioridad;
import model.vo.VOPelicula;

public class ColaPrioridadTest extends TestCase
{
	private ColaPrioridad<VOPelicula> cola;
	private DateFormat format= new SimpleDateFormat("d MMMM yyyy");
	private Date fecha;

	public void setupEscenario1(  )
	{
		cola = new ColaPrioridad<VOPelicula>( 6 );
	}

	public void setupEscenario2( )
	{
		setupEscenario1();

		VOPelicula pelicula1 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		pelicula1.setFechaLanzamiento(fecha);
		cola.insert(pelicula1);

		VOPelicula pelicula2 = new VOPelicula();
		try{ fecha = format.parse("20 Feb 1960");}catch(Exception e){e.printStackTrace();};
		pelicula2.setFechaLanzamiento(fecha);
		cola.insert(pelicula2);;

		VOPelicula pelicula3 = new VOPelicula();
		
		try{ fecha = format.parse("18 Mar 1990");}catch(Exception e){e.printStackTrace();};
		pelicula3.setFechaLanzamiento(fecha);
		
		cola.insert(pelicula3);

		VOPelicula pelicula4 = new VOPelicula();
		try{ fecha = format.parse("20 Abr 2012");}catch(Exception e){e.printStackTrace();};
		pelicula4.setFechaLanzamiento(fecha);
		cola.insert(pelicula4);

		VOPelicula pelicula5 = new VOPelicula();
		try{ fecha = format.parse("4 Jun 1985");}catch(Exception e){e.printStackTrace();};
		pelicula5.setFechaLanzamiento(fecha);
		cola.insert(pelicula5);

		VOPelicula pelicula6 = new VOPelicula();
		try{ fecha = format.parse("14 Sep 3020");}catch(Exception e){e.printStackTrace();};
		pelicula6.setFechaLanzamiento(fecha);
		cola.insert(pelicula6);
	}

	public void testInsert( )
	{
		setupEscenario1();

		VOPelicula pelicula1 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		pelicula1.setFechaLanzamiento(fecha);
		cola.insert(pelicula1);

		VOPelicula pelicula2 = new VOPelicula();
		try{ fecha = format.parse("20 Feb 1960");}catch(Exception e){e.printStackTrace();};
		pelicula2.setFechaLanzamiento(fecha);
		cola.insert(pelicula2);;

		VOPelicula pelicula3 = new VOPelicula();
		
		try{ fecha = format.parse("18 Mar 1990");}catch(Exception e){e.printStackTrace();};
		pelicula3.setFechaLanzamiento(fecha);
		
		cola.insert(pelicula3);

		VOPelicula pelicula4 = new VOPelicula();
		try{ fecha = format.parse("20 Abr 2012");}catch(Exception e){e.printStackTrace();};
		pelicula4.setFechaLanzamiento(fecha);
		cola.insert(pelicula4);

		VOPelicula pelicula5 = new VOPelicula();
		try{ fecha = format.parse("4 Jun 1985");}catch(Exception e){e.printStackTrace();};
		pelicula5.setFechaLanzamiento(fecha);
		cola.insert(pelicula5);

		assertFalse( cola.isEmpty() );
		assertEquals( 5, cola.size() );
		
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());
		
		try{ fecha = format.parse("20 Abr 2012");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());
		
		try{ fecha = format.parse("18 Mar 1990");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());
		
		try{ fecha = format.parse("4 Jun 1985");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());
		
		try{ fecha = format.parse("20 Feb 1960");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());
	}

	public void testDelMax( )
	{
		setupEscenario2();

		try{ fecha = format.parse("14 Sep 3020");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());
		
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());
		
		try{ fecha = format.parse("20 Abr 2012");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());
		
		try{ fecha = format.parse("18 Mar 1990");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());
		
		try{ fecha = format.parse("4 Jun 1985");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());
		
		try{ fecha = format.parse("20 Feb 1960");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, cola.delMax().getFechaLanzamiento());

		assertTrue( cola.isEmpty() );

	}	
}
