package test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;

public class ListaEncadenadaTest extends TestCase
{
	private ListaEncadenada<VOPelicula> lista;
	private DateFormat format= new SimpleDateFormat("d MMMM yyyy");
	private Date fecha;

	public void setupEscenario1(  )
	{
		lista = new ListaEncadenada<VOPelicula>( );
	}

	public void setupEscenario2( )
	{
		setupEscenario1();

		VOPelicula pelicula1 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		pelicula1.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula1);

		VOPelicula pelicula2 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		pelicula2.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula2);

		VOPelicula pelicula3 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		pelicula3.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula3);

		VOPelicula pelicula4 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		pelicula4.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula4);

		VOPelicula pelicula5 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		pelicula5.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula5);

		VOPelicula pelicula6 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 3020");}catch(Exception e){e.printStackTrace();};
		pelicula6.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula6);
	}

	@Test
	public void testInsert( )
	{
		setupEscenario1();
	


		VOPelicula pelicula1 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		pelicula1.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula1);

		VOPelicula pelicula2 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		pelicula2.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula2);

		VOPelicula pelicula3 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		pelicula3.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula3);

		VOPelicula pelicula4 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		pelicula4.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula4);

		VOPelicula pelicula5 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		pelicula5.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula5);

		assertFalse( lista.isEmpty() );
		assertEquals( 5, lista.darNumeroElementos() );
		
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		
		assertEquals(fecha, lista.darElemento(1).getFechaLanzamiento());

		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, lista.darElemento(2).getFechaLanzamiento());
		
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, lista.darElemento(3).getFechaLanzamiento());
		
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, lista.darElemento(4).getFechaLanzamiento());
		
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, lista.darElemento(5).getFechaLanzamiento());
	}

	@Test
	public void testEliminar( )
	{
		setupEscenario2();
		Date agno = lista.eliminarElemento(0).getFechaLanzamiento();
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, agno);
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, lista.eliminarElemento(0).getFechaLanzamiento());
		
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, lista.eliminarElemento(0).getFechaLanzamiento());
		
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, lista.eliminarElemento(0).getFechaLanzamiento());
		
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, lista.eliminarElemento(0).getFechaLanzamiento());
		
		try{ fecha = format.parse("2 Ago 3020");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, lista.eliminarElemento(0).getFechaLanzamiento());
		assertTrue( lista.isEmpty() );

	}

}
