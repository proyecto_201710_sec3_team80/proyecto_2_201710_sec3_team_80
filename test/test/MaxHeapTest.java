package test;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;
import model.data_structures.MaxHeap;
import model.vo.VOPelicula;

public class MaxHeapTest extends TestCase
{
	private MaxHeap maxHeap;
	private DateFormat format= new SimpleDateFormat("d MMMM yyyy");
	private Date fecha;
	
	public void setupEscenario1(  )
	{
		maxHeap = new MaxHeap( 4 );
	}
	
	public void setupEscenario2( )
	{
		setupEscenario1();
		

		VOPelicula pelicula1 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		pelicula1.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula1);

		VOPelicula pelicula2 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		pelicula2.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula2);

		VOPelicula pelicula3 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		pelicula3.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula3);

		VOPelicula pelicula4 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		pelicula4.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula4);

		VOPelicula pelicula5 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		pelicula5.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula5);

		VOPelicula pelicula6 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 3020");}catch(Exception e){e.printStackTrace();};
		pelicula6.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula6);
	}

	public void testInsert( )
	{
		setupEscenario1();
		
		VOPelicula pelicula1 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		pelicula1.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula1);

		VOPelicula pelicula2 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		pelicula2.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula2);

		VOPelicula pelicula3 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		pelicula3.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula3);

		VOPelicula pelicula4 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		pelicula4.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula4);

		VOPelicula pelicula5 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		pelicula5.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula5);
		
		assertFalse( maxHeap.isEmpty() );
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
	}
	
	public void testInsertWhenFull( )
	{
		setupEscenario2();
		
		VOPelicula pelicula1 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2017");}catch(Exception e){e.printStackTrace();};
		pelicula1.setFechaLanzamiento(fecha);
		maxHeap.insert(pelicula1);
		
		assertEquals(7, maxHeap.size());
	}
	
	public void testDelMax( )
	{
		setupEscenario2();
		try{ fecha = format.parse("2 Ago 3020");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, maxHeap.delMax().getFechaLanzamiento());
		assertTrue( maxHeap.isEmpty() );
		
	}
}
