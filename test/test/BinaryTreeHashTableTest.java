package test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import junit.framework.TestCase;
import model.data_structures.BinaryTreeHashTable;
import model.vo.VOPelicula;

public class BinaryTreeHashTableTest extends TestCase
{
	private BinaryTreeHashTable<String, VOPelicula> binaryTree;
	private DateFormat format = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
	private Date fecha;
	private Date fecha2;
	private Date fecha3;
	private Date fecha4;
	private Date fecha5;

	public void setupEscenario1( )
	{
		binaryTree = new BinaryTreeHashTable<String, VOPelicula>(5);
	}

	public void setupEscenario2( )
	{
		setupEscenario1();
		VOPelicula peliAux = new VOPelicula();
		try{fecha = format.parse("2 May 1998");}catch(Exception e){e.printStackTrace();};
		peliAux.setFechaLanzamiento(fecha);
		binaryTree.put("Gato", peliAux);
		try{fecha2 = format.parse("3 Jan 1665");}catch(Exception e){e.printStackTrace();};
		peliAux.setFechaLanzamiento(fecha2);
		binaryTree.put("Perro",peliAux);
		try{fecha3 = format.parse("6 Jul 2013");}catch(Exception e){e.printStackTrace();};
		try{peliAux.setFechaLanzamiento(fecha3);}catch(Exception e){e.printStackTrace();};
		binaryTree.put("Casa", peliAux);
		try{fecha4 = format.parse("3 Jan 2000");}catch(Exception e){e.printStackTrace();};
		peliAux.setFechaLanzamiento(fecha4);
		binaryTree.put("Carro", peliAux);
		try{fecha5 = format.parse("3 Jan 1665");}catch(Exception e){e.printStackTrace();};
		peliAux.setFechaLanzamiento(fecha5);
		binaryTree.put("Oso", peliAux);

	}

	public void testInsert( )
	{
		setupEscenario1();

		VOPelicula peliAux = new VOPelicula();
		try{fecha = format.parse("2 May 1998");}catch(Exception e){e.printStackTrace();};
		peliAux.setFechaLanzamiento(fecha);
		binaryTree.put("Gato", peliAux);
		try{fecha2 = format.parse("3 Jan 1665");}catch(Exception e){e.printStackTrace();};
		peliAux.setFechaLanzamiento(fecha2);
		binaryTree.put("Perro",peliAux);
		try{fecha3 = format.parse("6 Jul 2013");}catch(Exception e){e.printStackTrace();};
		try{peliAux.setFechaLanzamiento(fecha3);}catch(Exception e){e.printStackTrace();};
		binaryTree.put("Casa", peliAux);
		try{fecha4 = format.parse("3 Jan 2000");}catch(Exception e){e.printStackTrace();};
		peliAux.setFechaLanzamiento(fecha4);
		binaryTree.put("Carro", peliAux);
		try{fecha5 = format.parse("3 Jan 1665");}catch(Exception e){e.printStackTrace();};
		peliAux.setFechaLanzamiento(fecha5);
		binaryTree.put("Oso", peliAux);

		assertEquals( peliAux, binaryTree.get("Gato",fecha) );
		assertEquals( peliAux, binaryTree.get("Perro",fecha2) );
		assertEquals( peliAux, binaryTree.get("Casa",fecha3) );
		assertEquals( peliAux, binaryTree.get("Carro",fecha4) );
		assertEquals( peliAux, binaryTree.get("Oso",fecha5) );
		assertFalse( binaryTree.isEmpty() );
	}

	public void testEliminar( )
	{
		setupEscenario2();

		binaryTree.delete("Gato",fecha);
		binaryTree.delete("Perro",fecha2);
		binaryTree.delete("Casa", fecha3);
		binaryTree.delete("Carro",fecha4);
		binaryTree.delete("Oso",fecha5);
		assertTrue(binaryTree.isEmpty());
	}
}
