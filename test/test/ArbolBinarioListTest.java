package test;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import model.data_structures.ArbolBinarioList;
import model.data_structures.BinaryTreeHashTable;
import model.data_structures.BinaryTreeHashTableTags;
import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;
import model.vo.VOPeliculaUsuarios;
import model.vo.VOUsuario;

public class ArbolBinarioListTest {

	private ArbolBinarioList<Double, VOPeliculaUsuarios> binarioLista;
	private DateFormat format= new SimpleDateFormat("d MMMM yyyy");
	private Date fecha;
	private Date fecha2;
	private Date fecha3;
	private ListaEncadenada<String> generos;

	public void setupEscenario1()
	{
		binarioLista = new ArbolBinarioList<Double, VOPeliculaUsuarios>();
	}

	public void setupEscenario2(){
		binarioLista = new ArbolBinarioList<Double, VOPeliculaUsuarios>();

		VOPeliculaUsuarios peliculaUsuario1 = new VOPeliculaUsuarios();
		VOPeliculaUsuarios peliculaUsuario2 = new VOPeliculaUsuarios();
		VOPeliculaUsuarios peliculaUsuario3 = new VOPeliculaUsuarios();

		//Actores de la peli1
		String[] actores1 = new String[5]; 
		actores1[0] = "Line";
		actores1[1] = "Walt";
		actores1[2] = "Cameron";
		actores1[3] = "Leonardo";
		actores1[4] = "Ana";

		//Se crea el VOPelicula y se le asignan todos los datos
		VOPelicula peli1 = new VOPelicula();


		try{fecha = format.parse("2 Jul 2016");}catch(Exception e ){ e.printStackTrace();}

		generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("Action");
		peli1.setActores(actores1);
		peli1.setDirector("Vin diesel");
		peli1.setGenerosAsociados(generos);
		peli1.setIdPelicula(6);
		peli1.setNumeroRatings(5);
		peli1.setNumeroTags(3);
		peli1.setPromedioRatings(4);
		peli1.setRatingIMDB(9);

		//Tags Para la peli1
		ListaEncadenada<String> tags = new ListaEncadenada<String>();

		tags.agregarElementoFinal("Buena peli");
		tags.agregarElementoFinal("Excelente");
		tags.agregarElementoFinal("De lo mejor");

		peli1.setTagsAsociados(tags);
		peli1.setTitulo("Rain");

		BinaryTreeHashTableTags<String, VOUsuario> tablaUsuarios = new BinaryTreeHashTableTags<String, VOUsuario>(4);

		VOUsuario user1Peli1 = new VOUsuario();

		user1Peli1.setDiferenciaOpinion(12);
		user1Peli1.setIdUsuario(1259);
		user1Peli1.setNumRatings(12);
		user1Peli1.setPrimerTimestamp(1998);
		user1Peli1.setTag("Buena peli");
		tablaUsuarios.put(user1Peli1.getTag(), user1Peli1);

		VOUsuario user2Peli1 = new VOUsuario();

		user2Peli1.setDiferenciaOpinion(-1);
		user2Peli1.setIdUsuario(2031);
		user2Peli1.setNumRatings(2);
		user2Peli1.setPrimerTimestamp(2000);
		user2Peli1.setTag("Excelente");
		tablaUsuarios.put(user2Peli1.getTag(), user2Peli1);

		VOUsuario user3Peli1 = new VOUsuario();

		user3Peli1.setIdUsuario(78964);
		user3Peli1.setTag("De lo mejor");
		tablaUsuarios.put(user3Peli1.getTag(), user3Peli1);

		VOUsuario user4Peli1 = new VOUsuario();

		user4Peli1.setIdUsuario(16);
		user4Peli1.setTag("Muy mala");
		tablaUsuarios.put(user4Peli1.getTag(), user4Peli1);

		peliculaUsuario1.setPelicula(peli1);
		peliculaUsuario1.setUsuarios(tablaUsuarios);

		//Se crea la segunda pelicula que ir� en la estructura
		VOPelicula peli2 = new VOPelicula();

		//Actores de la peli2
		String[] actores2 = new String[4]; 
		actores2[0] = "Albert einstein";
		actores2[1] = "Isaac Newton";
		actores2[2] = "Michael Faraday";
		actores2[3] = "Ana Reina";

		try{fecha = format.parse("3 Dic 1997");}catch(Exception e ){ e.printStackTrace();}
		generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("Drama");
		peli2.setActores(actores2);
		peli2.setDirector("Marck Zuckemberg");
		peli2.setGenerosAsociados(generos);
		peli2.setIdPelicula(31416);
		peli2.setNumeroRatings(10);
		peli2.setNumeroTags(10);
		peli2.setPromedioRatings(5);
		peli2.setRatingIMDB(10);


		//Tags de la peli2
		tags = new ListaEncadenada<String>();

		tags.agregarElementoFinal("Buena peli");
		tags.agregarElementoFinal("Excelente");
		tags.agregarElementoFinal("De lo mejor");
		tags.agregarElementoFinal("La vi 5 veces");

		peli2.setTagsAsociados(tags);
		peli2.setTitulo("Clone");

		//Tabla de usuarios que han dado rating a la peli2
		tablaUsuarios = new BinaryTreeHashTableTags<String, VOUsuario>(5);

		VOUsuario user1Peli2 = new VOUsuario();

		user1Peli2.setIdUsuario(1259);
		user1Peli2.setTag("Buena peli");
		tablaUsuarios.put(user1Peli2.getTag(), user1Peli2);

		VOUsuario user2Peli2 = new VOUsuario();



		user2Peli2.setIdUsuario(2031);
		user2Peli2.setTag("Excelente");
		tablaUsuarios.put(user2Peli2.getTag(), user2Peli2);

		VOUsuario user3Peli2 = new VOUsuario();

		user3Peli2.setIdUsuario(78964);
		user3Peli2.setTag("De lo mejor");
		tablaUsuarios.put(user3Peli2.getTag(), user3Peli2);

		VOUsuario user4Peli2 = new VOUsuario();

		user4Peli2.setIdUsuario(16);
		user4Peli2.setTag("La vi 5 veces");
		tablaUsuarios.put(user4Peli2.getTag(), user4Peli2);

		VOUsuario user5Peli2 = new VOUsuario();
		user5Peli2.setIdUsuario(9864);
		user5Peli2.setTag("La pel�cula m�s mala de la historia");
		
		tablaUsuarios.put(user5Peli2.getTag(), user5Peli2);

		//Se inserta la pelicula y la 
		peliculaUsuario2.setPelicula(peli2);
		peliculaUsuario2.setUsuarios(tablaUsuarios);


		//Se crea la tercera pelicula que ir� en la estructura
		VOPelicula peli3 = new VOPelicula();

		//Actores de la peli3
		String[] actores3 = new String[6]; 
		actores3[0] = "einstein";
		actores3[1] = "Newton";
		actores3[2] = "Faraday";
		actores3[3] = "Reina";
		actores3[4] = "Bay";
		actores3[5] = "Hitler";

		try{fecha = format.parse("24 Ene 2020");}catch(Exception e ){ e.printStackTrace();}
		generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("Tragicomedia");

		peli3.setActores(actores3);
		peli3.setDirector("Socrates");
		peli3.setGenerosAsociados(generos);
		peli3.setIdPelicula(98566621);
		peli3.setNumeroRatings(2);
		peli3.setNumeroTags(2);
		peli3.setPromedioRatings(2);
		peli3.setRatingIMDB(2);


		//Tags de la peli3
		tags = new ListaEncadenada<String>();

		tags.agregarElementoFinal("Buena peli");
		tags.agregarElementoFinal("Excelente");
		tags.agregarElementoFinal("De lo mejor");
		tags.agregarElementoFinal("La vi 5 veces");
		tags.agregarElementoFinal("Los peores actores, no hacen bien su papel");
		tags.agregarElementoFinal("ahnuueunsuemasdf");

		peli2.setTagsAsociados(tags);
		peli2.setTitulo("Transformer 10");

		//Tabla de usuarios que han dado rating a la peli3
		tablaUsuarios = new BinaryTreeHashTableTags<String, VOUsuario>(5);

		VOUsuario user1Peli3 = new VOUsuario();

		user1Peli3.setIdUsuario(1259);
		user1Peli3.setTag("Buena peli");
		tablaUsuarios.put(user1Peli3.getTag(), user1Peli3);

		VOUsuario user2Peli3 = new VOUsuario();



		user2Peli3.setIdUsuario(2031);
		user2Peli3.setTag("Excelente");
		tablaUsuarios.put(user2Peli3.getTag(), user2Peli3);

		VOUsuario user3Peli3 = new VOUsuario();

		user3Peli3.setIdUsuario(78964);
		user3Peli3.setTag("De lo mejor");
		tablaUsuarios.put(user3Peli3.getTag(), user3Peli3);

		VOUsuario user4Peli3 = new VOUsuario();

		user4Peli3.setIdUsuario(16);
		user4Peli3.setTag("Los peores actores, no hacen bien su papel");
		tablaUsuarios.put(user4Peli3.getTag(), user4Peli3);

		VOUsuario user5Peli3 = new VOUsuario();
		user5Peli3.setIdUsuario(9864);
		user5Peli3.setTag("ahnuueunsuemasdf");
		tablaUsuarios.put(user5Peli3.getTag(), user5Peli3);

		//Se inserta la pelicula y la tabla de usuarios
		peliculaUsuario3.setPelicula(peli3);
		peliculaUsuario3.setUsuarios(tablaUsuarios);

		binarioLista.put(peliculaUsuario1.getPelicula().getRatingIMDB(),peliculaUsuario1);
		binarioLista.put(peliculaUsuario2.getPelicula().getRatingIMDB(), peliculaUsuario2);
		binarioLista.put(peliculaUsuario3.getPelicula().getRatingIMDB(), peliculaUsuario3);
	}

	@Test
	public void getTest(){
		setupEscenario1();

		assertNull("El arbol no deber�a tener elementos", binarioLista.get(new Double(9)));

		setupEscenario2();

		assertNotNull("El arbol deber�a tener un elemento asociado a la llave: 9", binarioLista.get(new Double(9)));
	}

	@Test
	public void putTest(){
		setupEscenario1();

		assertNull("No deber�a haber elementos asignados a la llave: 125", binarioLista.get(new Double(125)));

		VOPeliculaUsuarios peliculaUsuario1 = new VOPeliculaUsuarios();


		String[] actores = new String[4]; 
		actores[0] = "Carlos";
		actores[1] = "Luis";
		actores[2] = "Mat Daemon";
		actores[3] = "Leonardo Dicaprio";
		generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("Animation");
		//Se crea el VOPelicula y se le asignan todos los datos
		VOPelicula peli1 = new VOPelicula();
		peli1.setActores(actores);
		peli1.setDirector("Machete");
		peli1.setGenerosAsociados(generos);
		peli1.setIdPelicula(1);
		peli1.setNumeroRatings(9);
		peli1.setNumeroTags(4);
		peli1.setPromedioRatings(4.5);
		peli1.setRatingIMDB(125);

		ListaEncadenada<String> tags = new ListaEncadenada<String>();

		tags.agregarElementoFinal("Buena peli");
		tags.agregarElementoFinal("Excelente");
		tags.agregarElementoFinal("De lo mejor");
		tags.agregarElementoFinal("Recomendada");

		peli1.setTagsAsociados(tags);
		peli1.setTitulo("Toy Sory");

		BinaryTreeHashTableTags<String, VOUsuario> tablaUsuarios = new BinaryTreeHashTableTags<String, VOUsuario>(4);

		VOUsuario user1 = new VOUsuario();
		user1.setDiferenciaOpinion(12);
		user1.setIdUsuario(1259);
		user1.setNumRatings(12);
		user1.setPrimerTimestamp(1998);
		user1.setTag("Recomendada");
		tablaUsuarios.put(user1.getTag(), user1);

		peliculaUsuario1.setPelicula(peli1);
		peliculaUsuario1.setUsuarios(tablaUsuarios);

		binarioLista.put(peliculaUsuario1.getPelicula().getRatingIMDB(), peliculaUsuario1);

		assertNotNull("El �rbol deber�a tener un elemento asignado a la llave: 125", binarioLista.get(new Double(125)));


	}

	@Test
	public void deleteTest(){
		setupEscenario2();

		assertNotNull("Deber�a existir un elemento asociado a la llave: 2", binarioLista.get(new Double(2)));

		binarioLista.delete(new Double(2));

		assertNull("No deber�a existir un elemento asociado a la llave: 2", binarioLista.get(new Double(2)));
		binarioLista.delete(new Double(9));
		binarioLista.delete(new Double(10));

		assertEquals("El arbol deber�a estar vac�o", true, binarioLista.isEmpty());
	}

}
