package test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;
import model.data_structures.MaxHeap;
import model.data_structures.QueueUnaLlave;
import model.vo.VOPelicula;

public class QueueTest extends TestCase
{
	private QueueUnaLlave<VOPelicula> queue;
	private DateFormat format= new SimpleDateFormat("d MMMM yyyy");
	private Date fecha;

	
	public void setupEscenario1(  )
	{
		queue = new QueueUnaLlave<VOPelicula>( );
	}
	
	public void setupEscenario2( )
	{
		setupEscenario1();
		
		VOPelicula pelicula1 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		pelicula1.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula1);

		VOPelicula pelicula2 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		pelicula2.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula2);

		VOPelicula pelicula3 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		pelicula3.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula3);

		VOPelicula pelicula4 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		pelicula4.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula4);

		VOPelicula pelicula5 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		pelicula5.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula5);

		VOPelicula pelicula6 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 3020");}catch(Exception e){e.printStackTrace();};
		pelicula6.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula6);
	}

	public void testInsert( )
	{
		setupEscenario1();

		VOPelicula pelicula1 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		pelicula1.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula1);

		VOPelicula pelicula2 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		pelicula2.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula2);

		VOPelicula pelicula3 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		pelicula3.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula3);

		VOPelicula pelicula4 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		pelicula4.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula4);

		VOPelicula pelicula5 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		pelicula5.setFechaLanzamiento(fecha);
		queue.enqueue(pelicula5);
		
		assertFalse( queue.isEmpty() );
		assertEquals( 5, queue.size() );
		
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, queue.dequeue().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, queue.dequeue().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, queue.dequeue().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha,queue.dequeue().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, queue.dequeue().getFechaLanzamiento());
		
	}
	
	public void testDelMax( )
	{
		setupEscenario2();
		
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, queue.dequeue().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, queue.dequeue().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, queue.dequeue().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha,queue.dequeue().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, queue.dequeue().getFechaLanzamiento());
		try{ fecha = format.parse("2 Ago 3020");}catch(Exception e){e.printStackTrace();};
		assertEquals(fecha, queue.dequeue().getFechaLanzamiento());
		assertTrue( queue.isEmpty() );
		
	}

}
