package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.BinaryTreeHashList;
import model.data_structures.BinaryTreeHashTable;
import model.data_structures.BinaryTreeHashTableTags;
import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;
import model.vo.VOPeliculaUsuarios;
import model.vo.VOUsuario;

public class BinaryTreeHashListTest {

	private BinaryTreeHashList<String, VOPeliculaUsuarios> tablaBinario;
	private ListaEncadenada<String> generos;
	
	public void setupEscenario1(){
		tablaBinario = new BinaryTreeHashList<String, VOPeliculaUsuarios>();
	}

	public void setupEscenario2(){

		tablaBinario = new BinaryTreeHashList<String, VOPeliculaUsuarios>();

		VOPeliculaUsuarios peliUsuaio1 = new VOPeliculaUsuarios();

		VOPelicula peli1 = new VOPelicula();
		
		generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("Comedia");
		peli1.setGenerosAsociados(generos);
		peli1.setRatingIMDB(1245);

		BinaryTreeHashTableTags<String, VOUsuario> usuarios = new BinaryTreeHashTableTags<String, VOUsuario>(3);

		VOUsuario user1Peli1 = new VOUsuario();
		user1Peli1.setIdUsuario(1875);
		user1Peli1.setTag("Mala pelicula");
		usuarios.put(user1Peli1.getTag(), user1Peli1);
		VOUsuario user2Peli1 = new VOUsuario();
		user2Peli1.setIdUsuario(7777);
		user2Peli1.setTag("Es de lo peor, no la vean");
		usuarios.put(user2Peli1.getTag(), user2Peli1);
		VOUsuario user3Peli1 = new VOUsuario();
		user3Peli1.setIdUsuario(6328444);
		user3Peli1.setTag("Peor que animales fantasticos");
		usuarios.put(user3Peli1.getTag(), user3Peli1);
		
		
		peliUsuaio1.setPelicula(peli1);
		peliUsuaio1.setUsuarios(usuarios);

		tablaBinario.put(peliUsuaio1.getPelicula().getGeneros().darElementoPosicionActual(), peliUsuaio1);

		VOPeliculaUsuarios peliUsuaio2 = new VOPeliculaUsuarios();

		VOPelicula peli2 = new VOPelicula();
		
		generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("War");
		peli2.setGenerosAsociados(generos);
		peli2.setRatingIMDB(7845);

		BinaryTreeHashTableTags<String, VOUsuario> usuarios2 = new BinaryTreeHashTableTags<String, VOUsuario>(5);

		VOUsuario user1Peli2 = new VOUsuario();
		user1Peli2.setIdUsuario(1547);
		user1Peli2.setTag("Hoadsoj");
		usuarios2.put(user1Peli2.getTag(), user1Peli2);

		VOUsuario user2Peli2 = new VOUsuario();
		user2Peli2.setIdUsuario(8000);
		user2Peli2.setTag("safdsadfergsge");
		usuarios2.put(user2Peli2.getTag(), user2Peli2);

		VOUsuario user3Peli2 = new VOUsuario();
		user3Peli2.setIdUsuario(63287);
		user3Peli2.setTag("45s4sd4fww8888");
		usuarios2.put(user3Peli2.getTag(), user3Peli2);

		VOUsuario user4Peli2 = new VOUsuario();
		user4Peli2.setIdUsuario(748444);
		user4Peli2.setTag("sssaf8sfs8");
		usuarios2.put(user4Peli2.getTag(), user4Peli2);

		VOUsuario user5Peli2 = new VOUsuario();
		user5Peli2.setIdUsuario(00001);
		user5Peli2.setTag("asdfqqqqppofof000");
		usuarios2.put(user5Peli2.getTag(), user5Peli2);

		peliUsuaio2.setPelicula(peli2);
		peliUsuaio2.setUsuarios(usuarios2);

		tablaBinario.put(peliUsuaio2.getPelicula().getGeneros().darElementoPosicionActual(), peliUsuaio2);
		
		VOPeliculaUsuarios peliUsuaio3 = new VOPeliculaUsuarios();

		VOPelicula peli3 = new VOPelicula();
		
		generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("War");
		peli3.setGenerosAsociados(generos);
		peli3.setRatingIMDB(12450);

		BinaryTreeHashTableTags<String, VOUsuario> usuarios3 = new BinaryTreeHashTableTags<String, VOUsuario>(4);

		VOUsuario user1Peli3 = new VOUsuario();
		user1Peli3.setIdUsuario(9987);
		user1Peli3.setTag("siuiuru048");
		usuarios3.put(user1Peli3.getTag(), user1Peli3);

		VOUsuario user2Peli3 = new VOUsuario();
		user2Peli3.setIdUsuario(1425);
		user2Peli3.setTag("we5550");
		usuarios3.put(user2Peli3.getTag(), user2Peli3);

		VOUsuario user3Peli3 = new VOUsuario();
		user3Peli3.setIdUsuario(3338);
		user3Peli3.setTag("8888888");
		usuarios3.put(user3Peli3.getTag(), user3Peli3);

		VOUsuario user4Peli3 = new VOUsuario();
		user4Peli3.setIdUsuario(77410);
		user4Peli3.setTag("87see Hola");
		usuarios3.put(user4Peli3.getTag(), user4Peli3);
		
		peliUsuaio3.setPelicula(peli3);
		peliUsuaio3.setUsuarios(usuarios3);

		tablaBinario.put(peliUsuaio3.getPelicula().getGeneros().darElementoPosicionActual(), peliUsuaio3);

	}
	
	@Test
	public void getTest(){
		setupEscenario1();
		
		assertNull("No deber�a tener ningun elemento", tablaBinario.get("War").get(new Double(12450)));
		
		setupEscenario2();
		
		assertNotNull("Deber�a tener un elemento", tablaBinario.get("War").get(new Double(12450)));
		assertNotNull("Deber�a tener un elemento", tablaBinario.get("War").get(new Double(7845)));
		assertNotNull("Deber�a tener un elemento", tablaBinario.get("Comedia").get(new Double(1245)));

	}
	
	@Test
	public void putTest(){
		setupEscenario1();
		
		assertNull("No deber�a tener ningun elemento", tablaBinario.get("Terror").get(new Double(123456)));
		
		VOPeliculaUsuarios peliUsuaio1 = new VOPeliculaUsuarios();

		VOPelicula peli1 = new VOPelicula();
		
		generos = new ListaEncadenada<String>();
		generos.agregarElementoFinal("Terror");
		peli1.setGenerosAsociados(generos);
		peli1.setRatingIMDB(123456);

		BinaryTreeHashTableTags<String, VOUsuario> usuarios = new BinaryTreeHashTableTags<String, VOUsuario>(4);

		VOUsuario user1Peli3 = new VOUsuario();
		user1Peli3.setIdUsuario(9987);
		user1Peli3.setTag("S6 Edge");
		usuarios.put(user1Peli3.getTag(), user1Peli3);

		VOUsuario user2Peli3 = new VOUsuario();
		user2Peli3.setIdUsuario(1425);
		user2Peli3.setTag("630ssaa");
		usuarios.put(user2Peli3.getTag(), user2Peli3);

		VOUsuario user3Peli3 = new VOUsuario();
		user3Peli3.setIdUsuario(3338);
		user3Peli3.setTag("588s9 Michael");
		usuarios.put(user3Peli3.getTag(), user3Peli3);

		VOUsuario user4Peli3 = new VOUsuario();
		user4Peli3.setIdUsuario(77410);
		user4Peli3.setTag("88sq99");
		usuarios.put(user4Peli3.getTag(), user4Peli3);
		
		peliUsuaio1.setPelicula(peli1);
		peliUsuaio1.setUsuarios(usuarios);

		tablaBinario.put(peliUsuaio1.getPelicula().getGeneros().darElementoPosicionActual(), peliUsuaio1);
		
		assertNotNull("Deber�a tener un elemento", tablaBinario.get("Terror").get(new Double(123456)));
		
		inOrderTest();
	}
	
	public void inOrderTest(){
		setupEscenario2();
	 tablaBinario.get("War").inOrder();
	}
	
	@Test
	public void deleteTest(){
		
		setupEscenario2();
		assertNotNull("Deber�a existir un nodo con los datos: Genero = War y Rating = 7845", tablaBinario.get("War").get(new Double(7845)));
		tablaBinario.delete("War", new Double(7845));
		assertNull("No deber�a existir un nodo con los datos: Genero = War y Rating = 7845", tablaBinario.get("War").get(new Double(7845)));
		
	}
	

}
