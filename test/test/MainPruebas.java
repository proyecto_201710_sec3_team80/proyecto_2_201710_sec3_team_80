package test;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;

public class MainPruebas 
{
	private static ListaEncadenada<VOPelicula> lista;
	private static DateFormat format= new SimpleDateFormat("d MMMM yyyy");
	private static Date fecha;

	public static void setupEscenario1(  )
	{
		lista = new ListaEncadenada<VOPelicula>( );
	}

	public static void setupEscenario2( )
	{
		setupEscenario1();

		VOPelicula pelicula1 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2015");}catch(Exception e){e.printStackTrace();};
		pelicula1.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula1);

		VOPelicula pelicula2 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1960");}catch(Exception e){e.printStackTrace();};
		pelicula2.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula2);

		VOPelicula pelicula3 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1990");}catch(Exception e){e.printStackTrace();};
		pelicula3.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula3);

		VOPelicula pelicula4 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 2012");}catch(Exception e){e.printStackTrace();};
		pelicula4.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula4);

		VOPelicula pelicula5 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 1985");}catch(Exception e){e.printStackTrace();};
		pelicula5.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula5);

		VOPelicula pelicula6 = new VOPelicula();
		try{ fecha = format.parse("2 Ago 3020");}catch(Exception e){e.printStackTrace();};
		pelicula6.setFechaLanzamiento(fecha);
		lista.agregarElementoFinal(pelicula6);

		System.out.println(lista.darElemento(1).getFechaLanzamiento());
		System.out.println(lista.darElemento(2).getFechaLanzamiento());
		System.out.println(lista.darElemento(3).getFechaLanzamiento());
		System.out.println(lista.darElemento(4).getFechaLanzamiento());
		System.out.println(lista.darElemento(5).getFechaLanzamiento());
		System.out.println(lista.darElemento(6).getFechaLanzamiento());
	}


	public static void testEliminar( )
	{
		setupEscenario2();
		System.out.println("____________________________");
		System.out.println(lista.eliminarElemento(1).getFechaLanzamiento());
		System.out.println(lista.eliminarElemento(2).getFechaLanzamiento());
		System.out.println(lista.eliminarElemento(3).getFechaLanzamiento());
		System.out.println(lista.eliminarElemento(4).getFechaLanzamiento());
		System.out.println(lista.eliminarElemento(5).getFechaLanzamiento());
		System.out.println(lista.eliminarElemento(6).getFechaLanzamiento());
		//		assertEquals(2015, lista.eliminarElemento(1).getAnio());
		//		assertEquals(1960, lista.eliminarElemento(2).getAnio());
		//		assertEquals(1990, lista.eliminarElemento(3).getAnio());
		//		assertEquals(2012, lista.eliminarElemento(4).getAnio());
		//		assertEquals(1985, lista.eliminarElemento(5).getAnio());
		//		assertEquals(3020, lista.eliminarElemento(6).getAnio());
		//		assertTrue( lista.isEmpty() );

	}

	public static void main( String[] args )
	{
		testEliminar();
	}

}
