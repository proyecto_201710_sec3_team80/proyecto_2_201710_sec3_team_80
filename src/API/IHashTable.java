//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Sat Jan 28 06:55:20 EST 2017.

package API;

public interface IHashTable <Key , Value>{

	/**
	 * Retorna el valor asignado a la llave buscada	
	 * @param Key llave a buscar
	 * @return valor: Value  asociado a esa llave
	 */
	public Value get(Key key);

	/**
	 * Inserta una pareja llave-valor en la tabla
	 * @param Key llave asignada al valor
	 * @param val valor a insertar a la tabla
	 */
	public void put(Key Key, Value val);

	public Iterable<Key> keys();

	public void delete(Key key);

	/**
	 * Indica si la tabla contiene la llave o no
	 * @param key llave a buscar
	 */
	public boolean contains(Key key);

	/**
	 * Indica si la tabla est� vac�a
	 * @return true, si la tabla est� vac�a. false de lo contrar�o
	 */
	public boolean isEmpty();

	/**
	 * retorna la cantidad de elementos insertados en la tabla
	 * @return cantidad de elementos en la tabla
	 */
	public int size();

	

}

//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Wed Nov 2 05:27:55 EDT 2016.