package Controller;

import java.util.Date;

import API.ILista;
import model.data_structures.ArbolBinarioInfo;
import model.data_structures.ListaEncadenada;
import model.logic.Logica;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VOReporteSegmento;
import model.vo.VOUsuario;
import model.vo.VOUsuarioPelicula;

public class Controller {
	
	private static Logica logica = new Logica();
	
	public static boolean cargarDatos(){
		return logica.cargarDatos();
	}
	
	public static void ordenarPeliculasPorAnho(){
		logica.ordenarPeliculasPorAnho();
	}
	
	public static void registrarSolicitudRecomendacion(Integer idUsuario, String ruta){
		logica.registrarSolicitudRecomendacion(idUsuario, ruta);
	}
	
	public static void generarRespuestasRecomendaciones(){
		logica.generarRespuestasRecomendaciones();
	}
	
	public static ILista<VOPelicula> peliculasGenero(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal){
		return logica.peliculasGenero(genero, fechaInicial, fechaFinal);
	}
	
	public static void clasificarUsuariosPorSegmento(){
		logica.clasificarUsuariosPorSegmento();
	}
	
	public static ILista<VOPelicula> peliculasMayorPrioridad(int n){
		return logica.peliculasMayorPrioridad(n);
	}
	
	public static ILista<VOPelicula> consultarPeliculasFiltros(Integer anho, String pais, VOGeneroPelicula genero)
	{
		return logica.consultarPeliculasFiltros(anho, pais, genero);
	}
	
	public static ArbolBinarioInfo<String, String> getGeneros( )
	{
		return logica.getGeneros();
	}
	
	public static VOReporteSegmento generarReporteSegmento( String segmento )
	{
		return logica.generarReporteSegmento(segmento);
	}
	
	public static ListaEncadenada<VOUsuarioPelicula> informacionInteraccionUsuario( Long id)
	{
		return (ListaEncadenada<VOUsuarioPelicula>) logica.informacionInteraccionUsuario(id);
	}
	public static void agregarRatingConError( int idUsuario, int idPelicula, Double rating)
	{
		logica.agregarRatingConError(idUsuario, idPelicula, rating);
	}
}
