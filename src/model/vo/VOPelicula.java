package model.vo;

import java.text.Collator;
import java.util.Comparator;
import java.util.Date;

import model.data_structures.ListaEncadenada;
import model.vo.VORating.ComparatorByIDUsuario;

public class VOPelicula implements Comparable<VOPelicula>{

	private long idPelicula; 
	private String titulo;
	private Integer numeroRatings;
	private Integer numeroTags;
	private Double promedioRatings;
	private Date fechaLanzamineto;
	private String director;
	private String[] actores;
	private double ratingIMDB;
	private int votosTotales;  
	private double promedioAnualVotos; 
	private ListaEncadenada<String> generos;
	private ListaEncadenada<String> tagsAsociados;
	private double prioridad;
	private String pais;


	public void setPrioridad(double prioridad){
		this.prioridad = prioridad;
	}

	public double  getPrioridad(){
		return this.prioridad;
	}
	public Long getIdPelicula()  {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}

	public Integer getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public Integer getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public Double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}

	public ListaEncadenada<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ListaEncadenada<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ListaEncadenada<String> getGeneros() {
		return generos;
	}
	public void setGenerosAsociados(ListaEncadenada<String> generos) {
		this.generos = generos;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String[] getActores() {
		return actores;
	}

	public void setActores(String[] actores) {
		this.actores = actores;
	}

	public double getRatingIMDB() {
		return ratingIMDB;
	}

	public void setRatingIMDB(double ratingIMDB) {
		this.ratingIMDB = ratingIMDB;
	}

	public void setFechaLanzamiento(Date fechaLanzamiento){
		this.fechaLanzamineto = fechaLanzamiento;
	}

	public Date getFechaLanzamiento(){
		return fechaLanzamineto;
	}

	public void setVotosTotales(int votosTotales){
		this.votosTotales = votosTotales;
	}

	public int getVotosTotales(){
		return votosTotales;
	}

	public void setPromediAnualDeVotos(double promedioAnual){
		this.promedioAnualVotos = promedioAnual;
	}

	public double getPromedioAnualDeVotos(){
		return this.promedioAnualVotos;
	}
	public int compareTo1(Object w){

		VOPelicula temp = (VOPelicula) w;

		if(idPelicula < temp.getIdPelicula()){return -1;}else{return 1;}
	}


	public int mayorNumRating(VOPelicula b)
	{
		if(numeroRatings > b.numeroRatings){
			return 1;
		}
		else{return -1;}
	}

	public int ordenarMayorPromRating(VOPelicula b){
		if(promedioRatings > b.promedioRatings){
			return 1;
		}
		else if(promedioRatings == b.promedioRatings){return -0;}else{return -1;}
	}

	public void setPais(String p) {
		this.pais = p;
	}

	public String getPais() {
		return pais;
	}

	@Override
	public int compareTo(VOPelicula o) {
		if( idPelicula == o.idPelicula )
		{
			return 0;
		}
		else if( idPelicula < o.idPelicula )
		{
			return -1;
		}
		else
		{
			return 1;
		}

	}


	public ComparatorByAgno darComparadorAgno( )
	{
		return new ComparatorByAgno();
	}
	public class ComparatorByAgno implements Comparator<VOPelicula>
	{
		public ComparatorByAgno( )
		{

		}

		@Override
		public int compare(VOPelicula o1, VOPelicula o2) {
			if( o1.fechaLanzamineto == o2.fechaLanzamineto )
			{
				return 0;
			}
			else if( o1.fechaLanzamineto.after(o2.fechaLanzamineto) )
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}


	}

	public ComparatorAlfabetico darComparadorAlfabetico( )
	{
		return new ComparatorAlfabetico();
	}
	public class ComparatorAlfabetico implements Comparator<VOPelicula>
	{
		public ComparatorAlfabetico( )
		{

		}

		@Override
		public int compare(VOPelicula o1, VOPelicula o2) {
			return o1.titulo.compareToIgnoreCase(o2.titulo);
		}


	}

	public ComparatorRating darComparadorRating( )
	{
		return new ComparatorRating();
	}
	public class ComparatorRating implements Comparator<VOPelicula>
	{
		public ComparatorRating( )
		{

		}

		@Override
		public int compare(VOPelicula o1, VOPelicula o2) {
			if( o1.ratingIMDB == o2.ratingIMDB )
			{
				return 0;
			}
			else if( o1.ratingIMDB < o2.ratingIMDB )
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}

	}
<<<<<<< HEAD


	public int compararReq11(VOPelicula obj){

		int resul = -1;
		if(fechaLanzamineto.equals(obj.getFechaLanzamiento()) ){
			Collator comparator = Collator.getInstance();
			comparator.setStrength(comparator.PRIMARY);

			if(comparator.compare(getPais(), obj.getPais()) == 0){

				String genero;
				ListaEncadenada<String> generosPeli2 = obj.getGeneros();
				boolean diferentes = false;
				generos.cambiarActualPrimero();
				String genero2;

				for(int i =0; i < generos.darNumeroElementos()&& !diferentes; i++)
				{
					genero = generos.darElementoPosicionActual();
					generosPeli2.cambiarActualPrimero();

					for(int j=0; j < generosPeli2.darNumeroElementos() && !diferentes; j++){
						genero2 = generosPeli2.darElementoPosicionActual();
						if( genero2.compareTo(genero) != 0){resul = genero2.compareTo(genero); diferentes = true;}
						generosPeli2.avanzarSiguientePosicion();
					}
					generos.avanzarSiguientePosicion();
				}
				
				if(!diferentes){
					if(ratingIMDB == obj.getRatingIMDB()){resul = 0;}
					else if(ratingIMDB < obj.getRatingIMDB()){resul = -1;}
					else{resul = 1;}
				}
			}
			else if(comparator.compare(getPais(), obj.getPais()) == -1){resul = -1;}
			else{resul = 1;}


		}
		else if( fechaLanzamineto.compareTo(obj.getFechaLanzamiento()) < 0 ){resul = -1;}
		else{resul =  1;}

		return resul;
	}

=======
	
	public ComparatorBYIdPelicula darComparadorIdPelicula( )
	{
		return new ComparatorBYIdPelicula();
	}
	public class ComparatorBYIdPelicula implements Comparator<VOPelicula>
	{
		public ComparatorBYIdPelicula( )
		{
			
		}

		@Override
		public int compare(VOPelicula o1, VOPelicula o2) {
			if( o1.idPelicula == o2.idPelicula )
			{
				return 0;
			}
			else if( o1.idPelicula < o2.idPelicula )
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}

	
	}
	
>>>>>>> 088e56b838a58c700d5ba370a92a053c6fae8ca3
}

