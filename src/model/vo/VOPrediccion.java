package model.vo;

import java.util.Comparator;

import model.vo.VORating.ComparatorByIDPelicula;

public class VOPrediccion implements Comparable<VOPrediccion> 
{
	private long idPelicula;
	private long idUsuario;
	private double prediccion;
	
	public long getIDPelicula( )
	{
		return idPelicula;
	}
	
	public void setIDPelicula( long id )
	{
		this.idPelicula = id;
	}
	
	public long getIDUsuario( )
	{
		return idUsuario;
	}
	
	public void setIDUsuario( long id )
	{
		this.idUsuario = id;
	}
	
	public double getPrediccion( )
	{
		return prediccion;
	}
	
	public void setPrediccion( double sim )
	{
		this.prediccion = sim;
	}

	public ComparatorByRating darComparadorRating( )
	{
		return new ComparatorByRating();
	}
	
	public class ComparatorByRating implements Comparator<VOPrediccion>
	{
		public ComparatorByRating( )
		{
			
		}

		@Override
		public int compare(VOPrediccion o1, VOPrediccion o2) {
			if( o1.prediccion == o2.prediccion )
			{
				return 0;
			}
			else if( o1.prediccion > o2.prediccion )
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}

	
	}
	
	public ComparatorByIdPelicula darComparadorIDPelicula( )
	{
		return new ComparatorByIdPelicula();
	}
	
	public class ComparatorByIdPelicula implements Comparator<VOPrediccion>
	{
		public ComparatorByIdPelicula( )
		{
			
		}

		@Override
		public int compare(VOPrediccion o1, VOPrediccion o2) {
			if( o1.idPelicula == o2.idPelicula )
			{
				return 0;
			}
			else if( o1.idPelicula > o2.idPelicula )
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}

	
	}

	@Override
	public int compareTo(VOPrediccion o) {
		if( prediccion > o.prediccion )
		{
			return 1;
		}
		else if( prediccion < o.prediccion )
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
}
