package model.vo;

public class VOPeliculaPrioridad implements Comparable<VOPeliculaPrioridad> {
	
	private VOPelicula pelicula;
	private Double prioridad;
	
	public void setPelicula(VOPelicula pelicula){
		this.pelicula = pelicula;
	}
	
	public void setPrioridad(Double prioridad){
		this.prioridad = prioridad;
	}
	
	public VOPelicula getPelicula(){
		return pelicula;
	}
	
	public Double getPrioridad(){
		return prioridad;
	}

	@Override
	public int compareTo(VOPeliculaPrioridad o) {
		return prioridad.compareTo(o.getPrioridad());
	}
	
}
