package model.vo;

public class VOTag implements Comparable<VOTag> {
	private String tag;
	private long timestamp;
	private long idUsuario;
	private long idPelicula;
	
	/**
	 * contenido del tag
	 */
	private String contenido;
	
	public String getTag() {
		return contenido;
	}
	public void setTag(String tag) {
		this.contenido = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public void setIdUsuario(long idUsuario){
		this.idUsuario = idUsuario;
	}
	
	public void setIdPelicula(long idPelicula){
		this.idPelicula = idPelicula;
	}
	
	public long getIdPelicula(){
		return this.idPelicula;
	}
	
	public long getIdUsuario(){
		return idUsuario;
	}
	public boolean comparaTo(VOTag w){
		
		return(idPelicula < w.idPelicula);
	}
	
	@Override
	public int compareTo(VOTag w){

		if(idPelicula < w.getIdPelicula()){return -1;}else{return 1;}

	}
	
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	
	public int compareToIdUsuario(VOTag o){
		if(idUsuario < o.getIdUsuario())return -1;
		else if(idUsuario == o.getIdUsuario()) return 0;
		else return 1;
	}
	
}
