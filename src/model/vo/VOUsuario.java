package model.vo;

import java.util.Calendar;

import model.data_structures.ArbolBinarioInfo;

public class VOUsuario implements Comparable<VOUsuario> {

	private long idUsuario;	
	private long primerTimestamp;
	private int numRatings;
	private double diferenciaOpinion;
	private String tag;
	//Se usa para guardar los id's de las peliculas a las que les ha dado rating
	private ArbolBinarioInfo<Long,Long> ratingTopeliculas;
	
	//Se asigna unicamente cuando se cargan los usuarios desde el archivo CSV tags, hace referencia a idPelicula
	private Long idTagToPelicula;
	private int numeroTags;
	//Se asigna unicamente cuando se cargan los usuarios desde el archivo CSV raitinga, hace referencia a idPelicula
	private Long idPeliculaRaiting;
	
	public void setPeliculasRating(ArbolBinarioInfo<Long, Long> ratingToPelicula){
		this.ratingTopeliculas = ratingToPelicula;
	}
	
	public ArbolBinarioInfo<Long, Long> getPeliculasRating(){
		return this.ratingTopeliculas;
	}
	
	public void setIdPeliculaRaiting(Long idPeliculaRating){
		this.idPeliculaRaiting = idPeliculaRating;
	}

	public Long getIdPeliculaRaiting(){
		return this.idPeliculaRaiting;
	}

	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public void setNumeroTags(int numTags){
		this.numeroTags = numTags;
	}
	public int getNumeroTags(){
		return this.numeroTags;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	public void setTag(String tag){
		this.tag = tag;
	}
	public String getTag(){
		return this.tag;
	}

	public void setIdTagToPelicula(Long idTagToPelicula){
		this.idTagToPelicula = idTagToPelicula;
	}

	public Long getIdTagToPelicula(){
		return this.idTagToPelicula;
	}
	@Override
	public int compareTo(VOUsuario o) {

		if(idPeliculaRaiting != null){return idPeliculaRaiting.compareTo(o.getIdPeliculaRaiting());} else {return  idTagToPelicula.compareTo(o.getIdTagToPelicula());} 
	}

	public int compararSolicitudRecomendaion(VOUsuario o){

		int resultado =-1;
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTimeInMillis(primerTimestamp);
		int year = calendar1.get(Calendar.YEAR);
		int mes = calendar1.get(Calendar.MONTH);

		long yearMillis = year * 31556952L*1000;
		long mesMillis = (mes* (31556952L/12))*1000;
		long date1 = yearMillis + mesMillis;

		calendar1 = Calendar.getInstance();
		calendar1.setTimeInMillis(o.getPrimerTimestamp());
		year = calendar1.get(Calendar.YEAR);
		mes = calendar1.get(Calendar.MONTH);

		yearMillis = year * 31556952L*1000;
		mesMillis = (mes* (31556952L/12))*1000;
		
		long date2 = yearMillis + mesMillis;

		if(date1 < date2){resultado = -1;}
		else if (date1 == date2){resultado = compararRatings(o);}
		else{resultado = 1;}
		return resultado;
	}

	private int compararRatings(VOUsuario o){
		int resultado = -1;
		if(numRatings < o.getNumRatings()){resultado = -1;}
		else if(numRatings == o.getNumRatings()){resultado = compararTags(o);}
		else{resultado= 1;}
		return resultado;
	}


	private int compararTags(VOUsuario o){
		int resultado = -1;
		
		if(numeroTags < o.getNumeroTags()){resultado =  -1;}
		else if(numeroTags == o.getNumeroTags()){resultado = 0;}
		else{resultado = 1;}
		return resultado;
	}


}
