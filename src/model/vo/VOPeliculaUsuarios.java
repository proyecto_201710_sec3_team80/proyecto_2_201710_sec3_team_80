package model.vo;

import model.data_structures.BinaryTreeHashTable;
import model.data_structures.BinaryTreeHashTableTags;

public class VOPeliculaUsuarios implements Comparable<VOPeliculaUsuarios>{

	private VOPelicula pelicula;
	
    private BinaryTreeHashTableTags<String, VOUsuario> usuarios;
	
    public void setPelicula(VOPelicula pelicula){
    	this.pelicula = pelicula;
    }
    
    public void setUsuarios (BinaryTreeHashTableTags<String, VOUsuario> usuarios){
    	this.usuarios = usuarios;
    }
    
    public VOPelicula getPelicula(){
    	return this.pelicula;
    }
    
    public BinaryTreeHashTableTags<String, VOUsuario> getUsuarios(){
    	return this.usuarios;
    }

	@Override
	public int compareTo(VOPeliculaUsuarios o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
