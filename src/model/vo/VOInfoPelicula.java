package model.vo;

import java.util.Date;

public class VOInfoPelicula implements Comparable<VOInfoPelicula>{

	private String titulo;

	private Date fechaLanzamiento;

	private String director;

	private String[] actores;

	private double ratingIMDB;


	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String[] getActores() {
		return actores;
	}

	public void setActores(String[] actores) {
		this.actores = actores;
	}

	public double getRatingIMDB() {
		return ratingIMDB;
	}

	public void setRatingIMDB(double ratingIMDB) {
		this.ratingIMDB = ratingIMDB;
	}
	
	public void setFechaLanzamiento(Date fechaLanzamiento){
		this.fechaLanzamiento = fechaLanzamiento;
	}
	
	public Date getFechaLanzamiento(){
		return fechaLanzamiento;
	}

	@Override
	public int compareTo(VOInfoPelicula o) {

		return fechaLanzamiento.compareTo(o.getFechaLanzamiento());
	}



}
