package model.vo;

import java.util.Comparator;

import API.ILista;
import model.vo.VORating.ComparatorByIDPelicula;

/**
 * @author Venegas
 *
 */

public class VOUsuarioPelicula {
	
	/**
	 * nombre de la pel�cula
	 */

	private String nombrepelicula;
	
	/**
	 * rating dado por el usuario
	 */
	private double ratingUsuario;
	
	/**
	 * rating calculado por el sistema
	 */

	private double ratingSistema;
	
	/**
	 * error sobre el rating
	 */
	
	private double errorRating;
	
	/**
	 * list de tags generados por el usuario sobre la pelicula
	 */
	
	private ILista<VOTag> tags;
	
	/**
	 * id del usuario
	 */
	
	private long idUsuario;
	
	private long idPelicula;

	public VOUsuarioPelicula() {
		// TODO Auto-generated constructor stub
	}
	
	public String getNombrepelicula() {
		return nombrepelicula;
	}

	public void setNombrepelicula(String nombrepelicula) {
		this.nombrepelicula = nombrepelicula;
	}

	public double getRatingUsuario() {
		return ratingUsuario;
	}

	public void setRatingUsuario(double ratingUsuario) {
		this.ratingUsuario = ratingUsuario;
	}

	public double getRatingSistema() {
		return ratingSistema;
	}

	public void setRatingSistema(double ratingSistema) {
		this.ratingSistema = ratingSistema;
	}

	public double getErrorRating() {
		return errorRating;
	}

	public void setErrorRating(double errorRating) {
		this.errorRating = errorRating;
	}

	public ILista<VOTag> getTags() {
		return tags;
	}

	public void setTags(ILista<VOTag> tags) {
		this.tags = tags;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long id) {
		this.idUsuario = idUsuario;
	}

	public Long getIDPelicula() {
		return idPelicula;
	}

	public void setIDPelicula(Long id) {
		this.idPelicula = id;
	}
	

	public ComparatorByIDPeliculaUP darComparadorIDPelicula( )
	{
		return new ComparatorByIDPeliculaUP();
	}
	
	public class ComparatorByIDPeliculaUP implements Comparator<VOUsuarioPelicula>
	{
		public ComparatorByIDPeliculaUP( )
		{
			
		}

		public int compare(VOUsuarioPelicula o1, VOUsuarioPelicula o2) {
			if( o1.idPelicula == o2.idPelicula )
			{
				return 0;
			}
			else if( o1.idPelicula > o2.idPelicula )
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}

	
	}
	@Override
	public String toString(){
		return nombrepelicula;
	}
}
