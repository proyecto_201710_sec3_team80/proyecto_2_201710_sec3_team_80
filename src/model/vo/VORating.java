package model.vo;

import java.util.Comparator;

public class VORating implements Comparable<VORating> {

	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long timeStamp;
	private double error;
	private double prediccion;
	
	public void setPrediccion( double pred )
	{
		this.prediccion = pred;
	}
	public double getPrediccion( )
	{
		return prediccion;
	}
	public void setError(double error){
		this.error = error;
	}
	
	public double getError(){
		return this.error;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	
	public long getTimeStamp()
	{
		return timeStamp;
	}
	public void setTimeStamp(long time)
	{
	  this.timeStamp =time;
	}

	public boolean comparaToMovieId(VORating w){

		return(idPelicula < w.idPelicula);
	}

	public int compareTo1(Object w){

		VORating temp = (VORating) w;
		
		if(idPelicula < temp.getIdPelicula()){return -1;}else{return 1;}

	}
	
	@Override
	public int compareTo(VORating b)
	{
		if( idPelicula == b.getIdPelicula() ) 
		{
			return 0;
		}
		else if( idPelicula > b.getIdPelicula())
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	
	
	public ComparatorByIDPelicula darComparadorIDPElcila( )
	{
		return new ComparatorByIDPelicula();
	}
	
	public ComparatorByIDUsuario darComparadorIDUsuario( )
	{
		return new ComparatorByIDUsuario();
	}
	public class ComparatorByIDPelicula implements Comparator<VORating>
	{
		public ComparatorByIDPelicula( )
		{
			
		}

		@Override
		public int compare(VORating o1, VORating o2) {
			if( o1.idPelicula == o2.idPelicula )
			{
				return 0;
			}
			else if( o1.idPelicula > o2.idPelicula )
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}

	
	}
	
	public class ComparatorByIDUsuario implements Comparator<VORating>
	{
		public ComparatorByIDUsuario( )
		{
			
		}

		@Override
		public int compare(VORating o1, VORating o2) {
			if( o1.idUsuario == o2.idUsuario )
			{
				return 0;
			}
			else if( o1.idUsuario > o2.idUsuario )
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}

	
	}

}
