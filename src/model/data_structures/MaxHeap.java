//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Sat Jan 28 06:55:20 EST 2017.
package model.data_structures;

import API.IHeap;
import model.vo.VOInfoPelicula;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPrioridad;

public class MaxHeap implements IHeap <VOPeliculaPrioridad>{

	private VOPeliculaPrioridad[] pq;
	private int N =0;


	public MaxHeap(int maxN){
		pq = new VOPeliculaPrioridad[maxN+1];
	}
	@Override
	public boolean isEmpty() {
		return N ==0;
	}

	@Override
	public int size() {
		return N;
	}

	public void insert(VOPeliculaPrioridad pelicula) {
		if(pq.length == size()+1) resize();
		pq[++N] = pelicula;
		swim(N);
	
	}

	public void resize(){
		int tamanio = N; 
		VOPeliculaPrioridad[] temp = new VOPeliculaPrioridad[(tamanio+1)*2];
		for(int i =1; i < pq.length; i++){
			temp[i] = pq[i];
		}
		pq = temp;
	}
	
	@Override
	public VOPeliculaPrioridad delMax() {
		VOPeliculaPrioridad max = pq[1];
		exch(1,N--);
		pq[N+1] = null;
		sink(1);
		return max;
	}
	
	
	private boolean less(int i, int j){
		return pq[i].compareTo(pq[j])< 0;
	}
	
	private void exch(int i, int j){
		VOPeliculaPrioridad t = pq[i]; pq[i] =pq[j]; pq[j]= t; 
	}
	
	private void swim(int k){
		while(k > 1 && less(k/2,k)){
			exch(k/2,k);
			k =k/2;
		}
	}
	
	private void sink(int k){
		while(2*k <= N){
			int j= 2*k;
			if(j <N && less(j, j+1))j++;
			if(!less(k,j)) break;
			exch(k,j);
			k =j;
		}
	}
}