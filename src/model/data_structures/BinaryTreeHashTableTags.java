//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Sat Jan 28 06:55:20 EST 2017.

package model.data_structures;

import API.IHashTable;
import model.vo.VOUsuario;

public class BinaryTreeHashTableTags <Key extends Comparable<Key>, Value>{

	private int n;

	private int m;
	private String[] llaves;

	private ArbolBinarioInfo<Long, Value>[] binaryTree;

	public  BinaryTreeHashTableTags(int m) {
		// TODO Auto-generated constructor stub
		this.m = m;
		this.n = 0;

		binaryTree = (ArbolBinarioInfo<Long, Value>[]) new ArbolBinarioInfo[m];
		for (int i = 0; i < m; i++)
			binaryTree[i] = new ArbolBinarioInfo<Long, Value>();

	}

	private void resize(int chains) {
		BinaryTreeHashTableTags<Long, Value> temp = new BinaryTreeHashTableTags<Long, Value>(chains);
		for (int i = 0; i < m; i++) {
			for (Long key : binaryTree[i].keys()) {
				temp.put(key, binaryTree[i].get(key));
			}
		}
		this.m  = temp.m;
		this.n  = temp.n;
		this.binaryTree = temp.binaryTree;
	}

	private int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % m;
	} 


	public Value get(Key key, Long idUsuario) {
		
		if (key == null) throw new IllegalArgumentException("argument to get() is null");
        int i = hash(key);
        return binaryTree[i].get(idUsuario);
	}

	public void put(Key key, Value val) {
		 if (key == null) throw new IllegalArgumentException("first argument to put() is null");
	        if (val == null) {
	            delete(key,((VOUsuario)val).getIdUsuario());
	            return;
	        }

	        // double table size if average length of list >= 10
	        if (n >= 10*m) resize(2*m);

	        int i = hash(key);
	        if (!binaryTree[i].contains(((VOUsuario)val).getIdUsuario())) n++;
	        binaryTree[i].put(((VOUsuario)val).getIdUsuario(), val);

	}
	
	public void delete(Key key, Long idUsuario) {
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");

        int i = hash(key);
        if (binaryTree[i].contains(idUsuario)) n--;
        binaryTree[i].delete(idUsuario);

	}


	public Iterable<Long> keys() {
		 QueueUnaLlave<Long> queue = new QueueUnaLlave<Long>();
	        for (int i = 0; i < m; i++) {
	            for (Long key : binaryTree[i].keys())
	                queue.enqueue(key);
	        }
	        return queue;
	}

	
	public boolean contains(Key key, Long idUsuario) {
		if (key == null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key, idUsuario) != null;

	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public int size() {
		return n;
	}

}


//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Wed Nov 2 05:27:55 EDT 2016.
