package model.data_structures;

import java.util.Date;
import java.util.Iterator;
import java.util.Queue;

import model.vo.VOPelicula;

public class EncadenamientoSeparadoTH<K,V> 
{
	private ListaLlaveValorSecuencial<K, V>[] st;
	private int N;
	private int M;
	private K key;
	
	public EncadenamientoSeparadoTH(int m)
	{
		N = 0;
		this.M = m;
		st = (ListaLlaveValorSecuencial<K, V>[]) new ListaLlaveValorSecuencial[m];
		for(int i = 0; i < m; i++)
		{
			st[i]=new ListaLlaveValorSecuencial<K,V>();
		}
	}
	
	public K getKey( )
	{
		return key;
	}
	
	public int darTamanio()
	{
		int contador = 0;
		for( int i = 0; i < M; i++)
		{
			contador += st[i].size();
		}
		return contador;
	}
	
	public boolean estaVacia()
	{
		return N == 0;
	}
	
	public boolean tieneLlave(K llave)
	{
		if (llave == null) throw new IllegalArgumentException("argument to contains() is null");
        return darValor(llave) != null;
		
	}
	
	private int hash(K key)
	{
		return (int)(key.hashCode() & 0x7fffffff) % M; 
	}
	public V darValor(K key)
	{
		return (V) st[hash(key)].darValor(key); 
	}
	public boolean insertar(K key, V val)
	{
		if (N >= 10*M) resize(2*M);
		
		int i = hash(key);
        if (!st[i].tieneLlave(key)) N++;
        st[i].insertar(key, val);
        this.key = key;
        return true;
	}
	
	public int[] darLongitudListas( )
	{
		int[] resp = new int[M];
		for( int i = 0; i < M; i++)
		{
			resp[i] = st[i].size();
		}
		return resp;
	}
	 
//	public Iterable<K> keys()
//	{
//		return new MyIterable<K>();
//	}
//	
//	public class MyIterable<K> implements Iterable
//	{
//
//		@Override
//		public Iterator iterator() {
//			// TODO Auto-generated method stub
//			return new MyIterator<K>();
//		}
//		
//	}
	
//	public  class MyIterator<K> implements Iterator
//	{
//		private int posIter;
//		private Iterator iterador;
//		//private int siguientePos;
//
//		public MyIterator(){
//			posIter =0;
//			//siguientePos = posIter;
//			iterador = st[posIter].llaves().iterator();
//		}
//
//		public boolean hasNext() {
//
//			if( !iterador.hasNext()){
//				if(posIter >= st.length){
//					return false;
//				}
//				else{ 
//					boolean noNull = false; 
//					for(int i = posIter+1; i < st.length && !noNull ; i++){
//						if(st[i].estaVacia() == false){
//							noNull = true;
//							posIter = i;
//							iterador = st[posIter].llaves().iterator();
//						}
//					}
//					return noNull;
//				}
//			}
//			else{return true;}
//		}
//		@Override
//		public K next() {
//
//			K key = null;
//			if(hasNext()){
//				key = (K) iterador.next();
//			}
//			return key;
//
//		}
//		
//		
//	}
	
	public Iterable<K> keys() {
        QueueUnaLlave<K> queue = new QueueUnaLlave<K>();
        for (int i = 0; i < M; i++) {
            for (K key : st[i].keys())
                queue.enqueue(key);
        }
        return queue;
    } 
	
	private void resize(int chains) {
		EncadenamientoSeparadoTH<K, V> temp = new EncadenamientoSeparadoTH<K, V>(chains);
		for (int i = 0; i < M; i++) {
			
			for (K key : st[i].keys()) {
				temp.insertar(key, st[i].darValor(key));
			}
		}
		this.M  = temp.M;
		this.N  = temp.N;
		this.st = temp.st;

	}

	
}