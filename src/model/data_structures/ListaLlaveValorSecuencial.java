package model.data_structures;

import java.util.Iterator;
import java.util.Queue;


public class ListaLlaveValorSecuencial<K, V> 
{
	private Node first;
	private int size;
	
	public ListaLlaveValorSecuencial( )
	{
		first = null;
		size = 0;
	}
	
	public boolean estaVacia( )
	{
		return size == 0;
	}
	
	public boolean tieneLlave( K llave )
	{
		boolean encontro = false;
		Node actual = first;
		for( int i = 0; i < size && !encontro; i++ )
		{
			if( actual != null )
			{
				if( actual.llave == llave )
				{
					encontro = true;
				}
				else
				{
					actual = actual.next;	
				}
			}
		}
		return encontro;
	}
	
	public V darValor( K llave )
	{
		boolean encontro = false;
		Node actual = first;
		V valorBuscado = null;
		for( int i = 0; i < size() && !encontro; i++ )
		{
			if( actual != null )
			{
				if( (actual.llave).equals(llave) )
				{
					encontro = true;
					valorBuscado = actual.valor;
				}
				else
				{
					actual = actual.next;	
				}
			}
		}
		return valorBuscado;
	}

	public void insertar( K llave, V valor )
	{
		Node nuevo = new Node(llave, valor, first);
		first = nuevo;
	}
	
//	public Iterable<K> llaves( )
//	{
//		return new MyIterable<K>();
//	}
	
	
	public Node getFirst() {
		return first;
	}

	public int size(){
		Node actual = first;
		int contador = 0;
		while( actual != null )
		{
			contador++;
			actual = actual.next;
		}
		size = contador;
		return size;
	}
	
	public boolean isEmpty(){
		return size == 0;
	}
	
	public void deleteAtpos(int pos){
		Node previous = null;

		if(pos == 0){	    
	    	first = first.next;
	    	size--;
	    	return;
	    }
		
		int i = 0;
		Node current = first;
		
		while (i < pos && current != null){
		
			previous = current;
			current =current.next;
			
			i++;
		}
		
		size--;
		
	}
	
//	public class MyIterable<K> implements Iterable
//	{
//
//		@Override
//		public Iterator iterator() {
//			// TODO Auto-generated method stub
//			return new MyIterator<K>();
//		}
//		
//	}
//	
//	public class MyIterator<K> implements Iterator
//	{
//		private QueueUnaLlave<K> queue;
//	
//		public MyIterator( )
//		{
//			queue = new QueueUnaLlave<K>();
//			Node<K, V> actual = (Node<K, V>) first;
//			for( int i = 0; i < size;  i++ )
//			{
//				queue.enqueue(actual.getLlave());
//				actual = actual.getNext();
//			}
//		}
//
//		@Override
//		public boolean hasNext() 
//		{
//			return queue.size() != 0;
//		}
//
//		@Override
//		public K next() 
//		{
//			return queue.dequeue();
//		}
//		
//	}
	
	public Iterable<K> keys()  {
        QueueUnaLlave<K> queue = new QueueUnaLlave<K>();
        for (Node x = first; x != null; x = x.next)
            queue.enqueue(x.llave);
        return queue;
    }
	
	public K buscarLlave( K llave )
	{
		boolean encontro = false;
		Node actual = first;
		K buscado = null;
		for( int i = 0; i < size && !encontro; i++ )
		{
			if( ( actual.llave).equals(llave))
			{
				buscado = actual.llave;
				encontro = true;
			}
			else
			{
				actual = actual.next;	
			}	
		}
		return buscado;
	}
	
	public class Node 
	{

		private K llave;
		private V valor;
		private Node next;
		

		public Node() {
			
			this.llave = null;
			this.next = null;
		}
		
		
		public Node(K llave, V valor, Node next) 
		{
			this.llave = llave;
			this.valor = valor;
			this.next = next;
		}
			
	}
}
