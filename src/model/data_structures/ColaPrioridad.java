//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Sat Jan 28 06:55:20 EST 2017.

package model.data_structures;

import API.IHeap;
import model.vo.VOInfoPelicula;
import model.vo.VOPelicula;
import model.vo.VOUsuario;

public class ColaPrioridad <T extends Comparable<T>> implements IHeap<T> {

	private T[] pq;
	private int N =0;


	public ColaPrioridad(int maxN){
		pq = (T[]) new Comparable[maxN+1];
	}
	@Override
	public boolean isEmpty() {
		return N ==0;
	}

	@Override
	public int size() {
		return N;
	}

	public void insert(T pelicula) {
		if(pq.length == size()) resize();
		pq[++N] = pelicula;
		swim(N);
	
	}
	
	public void insertSolicitud(T item, int prioridad){
		if(pq.length == size()+1) resize();
		if(prioridad == 0)
			{((VOUsuario)item).setPrimerTimestamp(prioridad);}
			
		pq[++N] = item;
		swimSolicitud(N);
	}
	
	public void swimSolicitud(int k){
		while(k > 1 && lessSolicitud(k/2,k)){
			exch(k/2,k);
			k =k/2;
		}
	}
	
	public boolean lessSolicitud(int i, int j){
		return ((VOUsuario)pq[i]).compararSolicitudRecomendaion(((VOUsuario)pq[j])) <0;
	}
	public void resize(){
		int tamanio = N; 
		T[] temp = (T[])new Comparable[(tamanio+1)*2];
		for(int i =1; i < pq.length; i++){
			temp[i] = pq[i];
		}
		pq = temp;
	}

	@Override
	public T delMax() {
		T max = pq[1];
		exch(1,N--);
		pq[N+1] = null;
		sink(1);
		return max;
	}
	
	public T delMaxSolicitud(){
		T max = pq[1];
		exch(1,N--);
		pq[N+1] = null;
		sinkSolicitud(1);
		return max;
	}
	
	private void sinkSolicitud(int k){
		while(2*k <= N){
			int j= 2*k;
			if(j <N && lessSolicitud(j, j+1))j++;
			if(!lessSolicitud(k,j)) break;
			exch(k,j);
			k =j;
		}
	}
	
	
	private boolean less(int i, int j){
		return pq[i].compareTo(pq[j])< 0;
	}
	
	private void exch(int i, int j){
		T t = pq[i]; pq[i] =pq[j]; pq[j]= t; 
	}
	
	private void swim(int k){
		while(k > 1 && less(k/2,k)){
			exch(k/2,k);
			k =k/2;
		}
	}
	
	private void sink(int k){
		while(2*k <= N){
			int j= 2*k;
			if(j <N && less(j, j+1))j++;
			if(!less(k,j)) break;
			exch(k,j);
			k =j;
		}
	}

}
