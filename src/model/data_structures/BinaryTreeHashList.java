//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Sat Jan 28 06:55:20 EST 2017.


package model.data_structures;

import API.IHashTable;
import model.vo.VOPeliculaUsuarios;

public class BinaryTreeHashList <Key extends Comparable<Key>, Value>{

	private int n;

	private int m;

	private ArbolBinarioList<Double, Value>[] tree;

	public  BinaryTreeHashList() {
		// TODO Auto-generated constructor stub
		this.m = 30;
		this.n = 0;

		tree = (ArbolBinarioList<Double, Value>[]) new ArbolBinarioList[m];
		for (int i = 0; i < m; i++)
			tree[i] = new ArbolBinarioList<Double, Value>();

	}

	private int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % m;
	} 


	public ArbolBinarioList<Double, Value> get(Key key) {
		
		if (key == null) throw new IllegalArgumentException("argument to get() is null");
        int i = hash(key);
        return tree[i];
	}

	public void put(Key key, Value val) {
		 if (key == null) throw new IllegalArgumentException("first argument to put() is null");
	        if (val == null) {
	            delete(key, ((VOPeliculaUsuarios)(val)).getPelicula().getRatingIMDB());
	            return;
	        }


	        int i = hash(key);
	        if (!tree[i].contains(((VOPeliculaUsuarios)val).getPelicula().getRatingIMDB())) n++;
	        tree[i].put(((VOPeliculaUsuarios)val).getPelicula().getRatingIMDB(), val);

	}
	
	public void delete(Key key, Double rating) {
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");

        int i = hash(key);
        if (tree[i].contains(rating)) n--;
        tree[i].delete(rating);

	}


	public Iterable<Key> keys() {
		 QueueUnaLlave<Double> queue = new QueueUnaLlave<Double>();
	        for (int i = 0; i < m; i++) {
	            for (Double key : tree[i].keys())
	                queue.enqueue(key);
	        }
	        return queue;
	}

	
	public boolean contains(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key) != null;

	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public int size() {
		return n;
	}

}


//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Wed Nov 2 05:27:55 EDT 2016.
