//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Sat Jan 28 06:55:20 EST 2017.

package model.data_structures;

import java.util.Date;

import API.IHashTable;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;

public class BinaryTreeHashTable <Key extends Comparable<Key>, Value>{

	private int n;

	private int m;

	private ArbolBinarioInfo<Date, Value>[] binaryTree;

	public  BinaryTreeHashTable(int m) {
		// TODO Auto-generated constructor stub
		this.m = m;
		this.n = 0;

		binaryTree = (ArbolBinarioInfo<Date, Value>[]) new ArbolBinarioInfo[m];
		for (int i = 0; i < m; i++)
			binaryTree[i] = new ArbolBinarioInfo<Date, Value>();

	}

	private void resize(int chains) {
		BinaryTreeHashTable<Key, Value> temp = new BinaryTreeHashTable<Key, Value>(chains);
		for (int i = 0; i < m; i++) {
			
			for (Date key : binaryTree[i].keys()) {
				VOPelicula aux = (VOPelicula)binaryTree[i].get(key);
				temp.put((Key)aux.getGeneros().darElementoPosicionActual(), (Value)aux);
			}
		}
		this.m  = temp.m;
		this.n  = temp.n;
		this.binaryTree = temp.binaryTree;
	}

	private int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % m;
	} 


	public Value get(Key key, Date fecha) {
		
		if (key == null) throw new IllegalArgumentException("argument to get() is null");
        int i = hash(key);
        return binaryTree[i].get(fecha);
	}
	

	public void put(Key key, Value val) {
		 if (key == null) throw new IllegalArgumentException("first argument to put() is null");
	        if (val == null) {
	            delete(key, ((VOPelicula)val).getFechaLanzamiento());
	            return;
	        }

	        // double table size if average length of list >= 10
	        if (n >= 10*m) resize(2*m);

	        int i = hash(key);
	        if (!binaryTree[i].contains(((VOPelicula)val).getFechaLanzamiento())) n++;
	        binaryTree[i].put(((VOPelicula)val).getFechaLanzamiento(), val);

	}
	
	public void delete(Key key, Date fecha) {
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");

        int i = hash(key);
        if (binaryTree[i].contains(fecha)) n--;
        binaryTree[i].delete(fecha);

	}


	public Iterable<Key> keys() {
		 QueueUnaLlave<Date> queue = new QueueUnaLlave<Date>();
	        for (int i = 0; i < m; i++) {
	            for (Date key : binaryTree[i].keys())
	                queue.enqueue(key);
	        }
	        return queue;
	}

	
	public boolean contains(Key key, Date fecha) {
		if (key == null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key,fecha) != null;

	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public int size() {
		return n;
	}
	
	public ListaEncadenada<Value> getPeliculasRango(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal){
		
		int i = hash((Key)genero.getNombre());
		ArbolBinarioInfo<Date, Value> arbolAux = binaryTree[i];
		
		return arbolAux.llavesEnRango(fechaInicial, fechaFinal);
	}

}


//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Wed Nov 2 05:27:55 EDT 2016.
