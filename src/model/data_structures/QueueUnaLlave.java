package model.data_structures;

import java.util.Iterator;

import API.IQueue;

public class QueueUnaLlave<T> implements IQueue<T>, Iterable {

	private ListaEncadenada<T> list;
	
	public QueueUnaLlave(){
		list = new ListaEncadenada<T>();
	}
	
	public  void enqueue(T item){
		list.enqueue(item);
	}
	
	public T dequeue(){
		return list.dequeue();
	}
	
	public boolean isEmpty(){
		return list.isEmpty();
	}
	
	public int size(){
		return list.darNumeroElementos();
	}

	

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return list.iterator();
	}
	
}
