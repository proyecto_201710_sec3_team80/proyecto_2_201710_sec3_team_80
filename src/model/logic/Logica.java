package model.logic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import API.ILista;
import API.ISistemaRecomendacionPeliculas;
import model.data_structures.ArbolBinarioHash;
import model.data_structures.ArbolBinarioInfo;
import model.data_structures.ArbolBinarioList;
import model.data_structures.BinaryTreeHashList;
import model.data_structures.BinaryTreeHashTable;
import model.data_structures.BinaryTreeHashTableTags;
import model.data_structures.ColaPrioridad;
import model.data_structures.DiccionarioTerminos;
import model.data_structures.EncadenamientoSeparadoTH;
import model.data_structures.ListaEncadenada;
import model.data_structures.ListaLlaveValorSecuencial;
import model.data_structures.MaxHeap;
import model.vo.VOGeneroPelicula;
import model.vo.VOInfoPelicula;
import model.vo.VOPelicula;
import model.vo.VOPelicula.ComparatorAlfabetico;
import model.vo.VOPelicula.ComparatorByAgno;
import model.vo.VOPelicula.ComparatorRating;
import model.vo.VOPeliculaPrioridad;
import model.vo.VOPeliculaUsuarios;
import model.vo.VOPrediccion;
import model.vo.VOPrediccion.ComparatorByIdPelicula;
import model.vo.VOPrediccion.ComparatorByRating;
import model.vo.VORating;
import model.vo.VOReporteSegmento;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioPelicula;
import model.vo.VOUsuarioPelicula.ComparatorByIDPeliculaUP;
import model.vo.VORating.ComparatorByIDPelicula;
import model.vo.VORating.ComparatorByIDUsuario;
import test.ListaEncadenadaTest;

public class Logica implements ISistemaRecomendacionPeliculas{

	BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(System.out));;

	private final static String RUTA_JSON = "./data/links_json.json";
	private final static String RUTA_TAGS = "./data/tags.csv";
	private final static String RUTA_RATINGS = "./data/ratings.csv";
	private final static String RUTA_PELICULAS = "./data/movies.csv";
	private final static int SOLICITUD_ID = 1;
	private final static int SOLICITUD_JSON =0;
	private final static String RUTA_CATEGORIAS = "./data/categoriestagsv2.csv";
	private final static String NO_CLASIFICADO = "no-clasificado";
	private final static String NEUTRAL = "neutral";
	private final static String CONFORME = "conforme";
	private final static String INCONFORME = "inconforme";
	private final static String RUTA_SIMILITUDES = "similitudesFINAL.csv";

	//Carga el Json con las peliculas
	private CargadorJson cargar;
	//Se utiliza para cargar los ratings del archivo CSV "ratings"
	private ListaEncadenada<VORating> listaRatings;
	//Se utiliza para cargar los tags del archivo CSV "tags"
	private ListaEncadenada<VOTag> tags;
	//Se utiliza para cargar las peliculas del archivo CSV "movies"
	private ListaEncadenada<VOPelicula> peliculas;
	// Se utiliza para ordenar las peliculas 
	private QuickSort<VOPelicula> quickPelicula;
	//Se utiliza para ordenar los rating 
	private QuickSort<VORating> quickRating;
	//Se utiliza para ordenar los usuarios
	private QuickSort<VOUsuario> quickUsuario;
	//Se utiliza para ordenar las predicciones
	private QuickSort<VOPrediccion> quickPrediccion;
	//Se utiliza para almacenar los ratings despu�s de cargarlos 
	private VORating auxRating[];
	//Se utiliza para almacenar las peliculas despu�s de cargarlas
	private VOPelicula auxPeli[];
	//Se utiliza para ordenar los tags
	private QuickSort<VOTag> quickTag;
	//Se utiliza para almacenar los tags despu�s de cargados
	private VOTag auxTag[];
	//Se utiliza para cargar los usuarios que se leen del archivo CSV ratings
	private ListaEncadenada<VOUsuario> usuariosRating;
	//Se utiliza para cargar los usuarios que se leen del archivo CSV tags 
	private ListaEncadenada<VOUsuario> usuariosTags;
	//Se utiliza para almacenar los usuarios que se leen del archivo CSV tags
	private VOUsuario[] auxUsuariosTags;
	//Se utiliza para almacenar los usuarios que se leen del archivo CSV rating
	private VOUsuario[] auxUsuariosRating;
	//Se utiliza para implementar el �rbol del requerimiento 7
	private ArbolBinarioHash<Integer,VOPeliculaUsuarios> arbolRequerimiento7;
	//Se utiliza para registrar las solicitudes de recomendaci�n
	private ColaPrioridad<VOUsuario> colaSolicitudes;
	//Se usa para guardar los usuarios con los rating y los tags contados, el idTagToPelicula de estos usuarios  es = null
	private ArbolBinarioInfo<Long, VOUsuario> arbolUsuarios;
	//Se usa para guardar los usuarios cargados del CSV ratings, usando como llave el idUsuario lo cual garantiza que no existen usuarios repetidos
	private ArbolBinarioInfo<Long, VOUsuario> arbolUsuarioIdUsuarioRating;
	//Se usa para guardar los usuarios cargados del CSV tags, usando como llave el idUsuario
	private ArbolBinarioInfo<Long, VOUsuario> arbolUsuarioIdUsuarioTags;
	//Se usa para almacenar la peliculas
	private ArbolBinarioInfo<Long, VOPelicula> arbolPeliculas;
	//Se utiliza para saber qu� usuarios tienen solicitudes registradas
	private ArbolBinarioInfo<Integer, Integer> arbolIdUsuariosSolicitudes = new ArbolBinarioInfo<Integer, Integer>();
	//Se utiliza para saber qu� solicitudes provenientes de archivos Json ya fueron registradas en el sistema
	private ArbolBinarioInfo<String, String> arbolRutasJson = new ArbolBinarioInfo<String, String>();
	//Se utiliza para el requerimiento 3
	private BinaryTreeHashTable<String, VOPelicula> tablaRequerimiento3 = new BinaryTreeHashTable<String, VOPelicula>(4);
	//Se utiliza para almacenar las categorias de los tags
	private ArbolBinarioInfo<String,String> arbolCategorias;
	//Se utiliza para el requerimiento 6, diccionario de terminos
	private DiccionarioTerminos<String,Long> diccionario = new DiccionarioTerminos<>();
	//Se utiliza para el requerimiento 10
	private MaxHeap maxHeapReq10 = new MaxHeap(10);
	// Se utiliza para guardar las similitudes
	private EncadenamientoSeparadoTH<Long, EncadenamientoSeparadoTH<Long, Double>> similitudes = new EncadenamientoSeparadoTH<>(1000);
	// Se utiliza para guardar los usuarios conformes
	private ListaEncadenada<Long> conformes;
	// Se utiliza para guardar los usuarios inconformes
	private ListaEncadenada<Long> inConformes;
	// Se utiliza para guardar los usuarios neutrales
	private ListaEncadenada<Long> neutrales;
	// Se utiliza para guardar los usuarios no clasificados
	private ListaEncadenada<Long> noClasificados;
	//Se utiliza para guardar los ratings
	private ArbolBinarioInfo<Long, VORating> arbolRatings;
	//Se utiliza para guardar los géneros
	private ArbolBinarioInfo<String, String> generos;

	public boolean cargarDatos(){
		boolean cargado = false;

		if(cargarCSVs()){
			if(cargarJson(RUTA_JSON, auxPeli)){
				if(cargarCategorias(RUTA_CATEGORIAS)){
					if(cargarSimilitudes(RUTA_SIMILITUDES)){
						cargado = true;
					}
				}
			}
		}
		colaSolicitudes = new ColaPrioridad<VOUsuario>(4);
		cargarMaxHeap();
		calcularPrediccionesUsuario(300);
		//try{calcularSimilitudesPeliculas();}catch(Exception e){e.printStackTrace();}
		return cargado;
	}

	private boolean cargarJson(String rutaJson, VOPelicula[] auxPeli){
		cargar = new CargadorJson();
		return cargar.cargarPeliculas(auxPeli,tablaRequerimiento3);

	}

	private boolean cargarCSVs(){

		boolean cargado = false;
		if(cargarTagsSR(RUTA_TAGS)){
			if(cargarRatingsSR(RUTA_RATINGS)){
				if(cargarPeliculasSR(RUTA_PELICULAS)){
					cargado = true;
				}
			}
		}
		return cargado;
	}

	public boolean cargarCategorias(String rutaCategorias){
		boolean cargado = false;

		try{
			BufferedReader br = new BufferedReader(new FileReader(rutaCategorias));
			arbolCategorias = new ArbolBinarioInfo<String,String>();
			String line = br.readLine();

			String tag;
			String clasificacion;
			String[] dataSplit;
			while(line!=null){

				dataSplit = line.split(",");
				tag = dataSplit[0];
				clasificacion = dataSplit[1];
				arbolCategorias.put(tag, clasificacion);
				line = br.readLine();
			}

			if(arbolCategorias.size() != 0)
				cargado = true;

		}catch(Exception e){
			e.printStackTrace();
		}
		return cargado;
	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas){
		boolean cargo = false; 
		try{
			BufferedReader br = new BufferedReader(new FileReader(rutaPeliculas));
			peliculas = new ListaEncadenada<VOPelicula>();
			generos = new ArbolBinarioInfo<>();
			String line = br.readLine();
			VOPelicula temp;
			String data;
			String[] dataSplit;
			Long idPelicula;
			while(line!= null){

				temp = new VOPelicula();
				data = line;
				dataSplit = data.split(","); 

				if(isNumber(dataSplit[0])){
					idPelicula = Long.parseLong(dataSplit[0].replaceAll("\"",""));
					temp.setIdPelicula(idPelicula);
					temp.setGenerosAsociados(sacarGeneros(dataSplit));
					peliculas.agregarElementoFinal(temp);
				}
				line = br.readLine();
			}
			if(peliculas.darNumeroElementos() >0)
			{cargo =true;}
			//Se llama al m�todo para pasar las listas a arreglos
			pasarListasArreglo();
			br.close();

		}
		catch(IOException e){
			e.printStackTrace();
			cargo = false;
		}
		return cargo;

	}

	private boolean cargarSimilitudes(String rutaSimilitudes){
		boolean cargo = false;
		try{
			BufferedReader br = new BufferedReader(new FileReader(rutaSimilitudes));
			EncadenamientoSeparadoTH<Long, Double> segundaPeli;

			String line = br.readLine();
			String[] dataSplit;
			Long idPeli1;
			Long idPeli2;
			Double similitud;
			line = br.readLine();
			while(line != null){

				dataSplit = line.split(",");
				idPeli1 = Long.parseLong(dataSplit[0]);
				idPeli2 = Long.parseLong(dataSplit[1]);
				similitud = Double.parseDouble(dataSplit[2]);
				segundaPeli = new EncadenamientoSeparadoTH<>(100);

				if(!similitudes.tieneLlave(idPeli1)){
					segundaPeli.insertar(idPeli2, similitud);
					similitudes.insertar(idPeli1, segundaPeli);
				}else
				{
					segundaPeli = similitudes.darValor(idPeli1); 
					segundaPeli.insertar(idPeli2, similitud);
				}
				
				line = br.readLine();
			}
		}
		catch(IOException e){
			e.printStackTrace();
			cargo = false;
		}

		return cargo;
	}


	private ListaEncadenada<String> sacarGeneros(String[] dataSplit){
		ListaEncadenada<String> generosT = new ListaEncadenada<String>();

		String[] generosSplit = dataSplit[dataSplit.length-1].split("[|]");
		for(String tempo: generosSplit){
			generosT.agregarElementoFinal(tempo.replaceAll("\"", ""));
			generos.put(tempo.replaceAll("\"", ""), tempo.replaceAll("\"", ""));
		}

		return generosT;
	}

	private void pasarListasArreglo(){

		//Se pasa la lista de ratings a una lista auxiliar

		ListaEncadenada<VORating> listaRating = (ListaEncadenada<VORating>)listaRatings;

		//se inicializa el atributo con auxrating con el tama�o de la listaRatings

		auxRating = new VORating[listaRating.darNumeroElementos()];

		// Se cambia el elemento actual de la lista para el primero **Esto se hace por precauci�n, posiblemento no es necesar�o** 

		listaRating.cambiarActualPrimero();

		//Se pasan los elementons de la lista, empezando desde el primer elemento y avanzando con los apuntadores para no tener que 
		//avanzar buscando la posici�n i de la lista. lo cu�l har�a que se volviera de muy lento.

		for(int i =0; i < listaRating.darNumeroElementos();i++){
			auxRating[i] = listaRating.darElementoPosicionActual();
			listaRating.avanzarSiguientePosicion();
		}

		//Se pasa la lista de peliculas a una lista auxiliar

		ListaEncadenada<VOPelicula> listaPelicula = (ListaEncadenada<VOPelicula>)peliculas;

		//Se cambia el elemento actual, de la lista de peliculas, al primer elemento  ** De nuevo, s�lo se hace por precauci�n*

		listaPelicula.cambiarActualPrimero();

		//Se inicializa el arreglo de pel�culas del tama�o de la lista de pel�culas

		auxPeli = new VOPelicula[listaPelicula.darNumeroElementos()];

		//Se asigna las peliculas de la lista de la misma manera que con los ratings

		VOPelicula voPeliculaAuxiliar;
		for(int i =0; i < listaPelicula.darNumeroElementos();i++){
			auxPeli[i] = listaPelicula.darElementoPosicionActual();
			listaPelicula.avanzarSiguientePosicion();
		}


		ListaEncadenada<VOTag> listaTags = (ListaEncadenada<VOTag>)tags;

		listaTags.cambiarActualPrimero();

		auxTag = new VOTag[listaTags.darNumeroElementos()];

		for(int i =0; i < listaTags.darNumeroElementos(); i++){
			auxTag[i] = listaTags.darElementoPosicionActual();
			listaTags.avanzarSiguientePosicion();
		}

		auxUsuariosTags = new VOUsuario[usuariosTags.darNumeroElementos()];

		usuariosTags.cambiarActualPrimero();

		for(int i = 0; i < usuariosTags.darNumeroElementos(); i++){
			auxUsuariosTags[i] = usuariosTags.darElementoPosicionActual();
			usuariosTags.avanzarSiguientePosicion();
		}

		quickUsuario = new QuickSort<VOUsuario>();
		quickUsuario.sort(auxUsuariosTags);


		auxUsuariosRating = new VOUsuario[usuariosRating.darNumeroElementos()];

		usuariosRating.cambiarActualPrimero();

		for(int i =0; i < usuariosRating.darNumeroElementos(); i++){
			auxUsuariosRating[i] = usuariosRating.darElementoPosicionActual();
			usuariosRating.avanzarSiguientePosicion();
		}

		quickUsuario.sort(auxUsuariosRating);


		asignarRatings();
		asignarTags();
		pasarDatosArbol();
		contarTagsYRating();
	}
	
	private VOPrediccion[] pasarPrediccionesArreglo( ListaEncadenada<VOPrediccion> prediccionesList )
	{
		//Se pasa la lista de ratings a una lista auxiliar

				ListaEncadenada<VOPrediccion> listaPrediccion = (ListaEncadenada<VOPrediccion>)prediccionesList;

				//se inicializa el atributo con auxrating con el tama�o de la listaRatings

				VOPrediccion[] auxPrediccion = new VOPrediccion[listaPrediccion.darNumeroElementos()];

				// Se cambia el elemento actual de la lista para el primero **Esto se hace por precauci�n, posiblemento no es necesar�o** 

				listaPrediccion.cambiarActualPrimero();

				//Se pasan los elementons de la lista, empezando desde el primer elemento y avanzando con los apuntadores para no tener que 
				//avanzar buscando la posici�n i de la lista. lo cu�l har�a que se volviera de muy lento.

				for(int i =0; i < listaPrediccion.darNumeroElementos();i++){
					auxPrediccion[i] = (VOPrediccion) listaPrediccion.darElementoPosicionActual();
					listaPrediccion.avanzarSiguientePosicion();
				}
				
				quickPrediccion = new QuickSort<VOPrediccion>();
				quickPrediccion.sort(auxPrediccion);
				
		return auxPrediccion;
	}

	private ListaEncadenada<VOPrediccion> pasarPrediccionesLista( VOPrediccion[] prediccionesArreglo )
	{
		//Se pasa la lista de ratings a una lista auxiliar

		ListaEncadenada<VOPrediccion> listaPrediccion = new ListaEncadenada<>();

		//se inicializa el atributo con auxrating con el tama�o de la listaRatings

		VOPrediccion[] auxPrediccion = prediccionesArreglo;

		// Se cambia el elemento actual de la lista para el primero **Esto se hace por precauci�n, posiblemento no es necesar�o** 


		//Se pasan los elementons de la lista, empezando desde el primer elemento y avanzando con los apuntadores para no tener que 
		//avanzar buscando la posici�n i de la lista. lo cu�l har�a que se volviera de muy lento.

		for( int i = 0; i < auxPrediccion.length; i++ )
		{
			listaPrediccion.agregarElementoFinal(auxPrediccion[i]);
		}
		
		return listaPrediccion;
	}

	private void asignarRatings(){


		//Se inicializa el atributo para ordenar el arreglo de ratings
		quickRating = new QuickSort<VORating>();
		//Se ordena el arreglo de ratings 
		quickRating.sort(auxRating);

		//se inicializa el atributo para ordenar las pel�culas
		quickPelicula = new QuickSort<VOPelicula>();
		quickPelicula.sort(auxPeli);

		//cuenta la cantidad de ratings que tiene una pelicula
		int contador;
		//Id de la pel�cula a la cu�l se le est� contando los ratings
		long idPelicula;
		//Indica si se cambia de id para comenzar a contar los de otra pel�cula
		boolean cambioId;

		double promedioRating;
		//La posici�n en la cual empiezan los ratings de una pel�cula
		int pos = 0;
		//Se crea un objeto temporal de tipo VOPelicula
		VOPelicula tempo = new VOPelicula();

		//Este for recorre el arreglo de pel�culas 
		for(int i = 0; i < auxPeli.length; i++){
			//Se inicializa el contador de pel�culas en 0 as� cuando una pel�cula no tenga ratings sigue quedando en 0
			contador =0;
			promedioRating =0.0;
			// Se le asigna la pel�cula de la posici�n i al objeto tempo 
			tempo =auxPeli[i];

			//Se obtiene el id de la pelicula en el objeto tempo
			idPelicula = tempo.getIdPelicula();

			cambioId  = false;

			//Este for recorre el arreglo de ratings, se detiene cuando se acaban los ratigns o cuando se cambia de un id a otro
			for(int j = pos; j < auxRating.length && !cambioId; j++){

				VORating auxRa = auxRating[j];
				//Se verifica que el id de la pelicula en el objeto tempo sea igual al id de la pel�cula en la posici�n j de los ratings
				if(idPelicula == auxRa.getIdPelicula()){

					contador++;
					promedioRating += auxRa.getRating();
				}
				//Si los dos id anteriores no son iguales
				else
				{
					// Se c�lcula la siguiente posici�n en el arreglo de las pel�culas
					int siguientePos = i+1;
					//Se asigna true a la variable cambioId se�alando que los ratings siguientes pertenecen otra pel�cula
					cambioId = true;

					try{
						//Se verifica si el rating siguiente el de la siguiente pel�cula 
						if(auxPeli[siguientePos].getIdPelicula() != auxRating[j].getIdPelicula()){
							//Se�ala que el id del rating es iguale a el id de la pel�cula 
							boolean idIgual = false;
							//Se recorre el arreglo de ratings hasta encontrar un rating que tengan el mismo id de la pel�cula 
							for(int h =j; h < auxRating.length && !idIgual ; h++){


								//Si se encuentra un rating con el mismo id de la pelicula
								if(auxPeli[siguientePos].getIdPelicula() == auxRating[h].getIdPelicula()){
									//Se se�ala que se encontraron dos id iguales
									idIgual = true;

									//Se hace que j sea igual a h para que al iniciar a contar empiece en la posici�n con los ratings conrrectos
									j = h;
								}

							}
						}
					}/*sucede cuando se acaba la lista de ratings o cuando hay alg�n error*/catch(Exception e){return;}

					//Se hace para que el segundo for inicie en la posici�n en la que se encuentrar los rating de la siguiente pel�cula
					pos =j;
				}

			}
			//Se asigna la cantidad de rating a la pel�cula
			tempo.setNumeroRatings(contador);
			if(contador!=0){
				double promedio = promedioRating/contador;
				tempo.setPromedioRatings(promedio);
			}
			//	System.out.println("idPelicula: "+ tempo.getIdUsuario() + " Posici�n en el arreglo pelis " +i + " Numero de ratings: "+ tempo.getNumeroRatings() + " Posici�n en el arreglo rating: " + pos);
		}

	}

	private void asignarTags(){
		quickTag = new QuickSort<VOTag>();
		quickTag.sort(auxTag);

		int contador;
		ListaEncadenada<String> listaTags;
		boolean cambioId;
		int pos =0;
		String tag;
		VOPelicula peliTempo = new VOPelicula();
		VOTag tagTempo = new VOTag();
		int posJ =0;
		long idPelicula;
		long idTag;

		for(int i =pos; i < auxPeli.length; i++){

			contador =0;
			peliTempo = auxPeli[i];
			idPelicula = peliTempo.getIdPelicula();
			tag = "";
			cambioId = false;
			listaTags = new ListaEncadenada<String>();

			for(int j = posJ; j < auxTag.length && !cambioId; j++)
			{

				tagTempo = auxTag[j];
				idTag = tagTempo.getIdPelicula();

				if(idTag == idPelicula){
					contador++;
					tag = tagTempo.getTag();
					listaTags.agregarElementoFinal(tag);
				}
				else
				{
					cambioId = true;

					int siguientePos = i+1;
					try{
						if(auxTag[j].getIdPelicula() != auxPeli[siguientePos].getIdPelicula())
						{
							boolean mismoId = false;
							for(int h=i; h < auxPeli.length && !mismoId; h++){

								if(auxTag[j].getIdPelicula() == auxPeli[h].getIdPelicula())
								{
									mismoId = true;
									i =h-1;

								}
							}

						}
					}catch(Exception e){return;}
					pos = i;
					posJ = j;

				}

			}	
			peliTempo.setNumeroTags(contador);
			peliTempo.setTagsAsociados(listaTags);
			//	System.out.println("Numero de tags: "+ peliTempo.getNumeroTags() + " posici�n en arregloTag: " + posJ);
		}
	}

	private boolean isNumber(String dato){
		try{
			Double d = Double.parseDouble(dato);
		}
		catch(NumberFormatException e){
			String agno= dato.split("-")[0];

			try{
				Double dou = Double.parseDouble(agno);

			}
			catch(NumberFormatException e2)
			{
				String partidoComilla = dato.replaceAll("\"","");
				try{
					Double dou = Double.parseDouble(partidoComilla);
				}
				catch(Exception e3){return false;}
				return true;
			}
			return true;
		}
		return true;
	}

	private void pasarDatosArbol(){

		arbolUsuarioIdUsuarioTags = new ArbolBinarioInfo<>();
		arbolPeliculas = new ArbolBinarioInfo<>();
		arbolUsuarioIdUsuarioRating = new ArbolBinarioInfo<>();


		for(int i =0; i < auxUsuariosTags.length; i++ ){
			arbolUsuarioIdUsuarioTags.put(auxUsuariosTags[i].getIdUsuario(), auxUsuariosTags[i]);
		}

		for(int i =0; i < auxPeli.length; i++ ){
			arbolPeliculas.put(auxPeli[i].getIdPelicula(), auxPeli[i]);
		}

		for(int i =0; i < auxUsuariosRating.length; i++){
			arbolUsuarioIdUsuarioRating.put(auxUsuariosRating[i].getIdUsuario(), auxUsuariosRating[i]);
		}
	}


	private void contarTagsYRating(){
		int numeroTags;
		int numeroRating;
		Long idUsuarioUser;
		VOUsuario usuarioAux;
		Long idPeliculaRating;
		ListaEncadenada<VOUsuario> listaUser = new ListaEncadenada<>();
		ArbolBinarioInfo<Long,Long> peliculas;
		arbolUsuarios = new ArbolBinarioInfo<>();
		arbolUsuarioIdUsuarioRating.inOrderLista(listaUser);

		listaUser.cambiarActualPrimero();
		for(int j =0; j < listaUser.darNumeroElementos(); j++){

			usuarioAux = listaUser.darElementoPosicionActual();
			idUsuarioUser = usuarioAux.getIdUsuario();
			numeroRating =0;
			peliculas = new ArbolBinarioInfo<Long,Long>();
			for(int i =0; i < auxRating.length; i++ ){
				idPeliculaRating = auxRating[i].getIdUsuario();
				if(idUsuarioUser.longValue() == idPeliculaRating)
				{
					numeroRating++;
					peliculas.put(idPeliculaRating,idPeliculaRating);
				}
			}
			usuarioAux.setPeliculasRating(peliculas);

			numeroTags =0;
			for(int h =0; h < auxTag.length; h++ ){

				if(idUsuarioUser.longValue() == auxTag[h].getIdUsuario())
					numeroTags++;
			}

			usuarioAux.setNumeroTags(numeroTags);
			usuarioAux.setNumRatings(numeroRating);
			listaUser.avanzarSiguientePosicion();
		}

		arbolUsuarios = arbolUsuarioIdUsuarioRating;

	}

	@Override
	public boolean cargarRatingsSR(String rutaRaitings) {
		boolean cargado = false;

		listaRatings  = new ListaEncadenada<VORating>();
		usuariosRating = new  ListaEncadenada<>();

		try{
			BufferedReader br = new BufferedReader(new FileReader(rutaRaitings));

			String line = br.readLine();
			VORating temp;
			String data;
			String[] dataSplit;
			long idUsuario;
			long idPelicula;
			double rating;
			VOUsuario tempUser;
			while(line!= null){

				temp = new VORating();
				data = line;
				dataSplit = data.split(",");
				tempUser = new VOUsuario();

				if(isNumber(dataSplit[0])){
					idUsuario = Integer.parseInt(dataSplit[0]);
					idPelicula = Integer.parseInt(dataSplit[1]);
					rating = Double.parseDouble(dataSplit[2]);
					temp.setIdUsuario(idUsuario);
					temp.setIdPelicula(idPelicula);
					temp.setRating(rating);
					temp.setTimeStamp(new Long(dataSplit[3]));
					temp.setError(Double.parseDouble(dataSplit[4]));
					listaRatings.agregarElementoFinal(temp);

					tempUser.setIdPeliculaRaiting(idPelicula);
					tempUser.setIdUsuario(idUsuario);
					tempUser.setPrimerTimestamp(new Long(dataSplit[3]));

					usuariosRating.agregarElementoFinal(tempUser);
				}
				line = br.readLine();
			}
			cargado = true;
			br.close();

		}
		catch(IOException e){
			e.printStackTrace();
			cargado = false;
		}

		return cargado;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		boolean cargado = false;

		tags = new ListaEncadenada<VOTag>();
		usuariosTags = new ListaEncadenada<VOUsuario>();
		try{
			BufferedReader br = new BufferedReader(new FileReader(rutaTags));

			String line = br.readLine();

			VOTag temp;
			String data;
			String[] dataSplit;
			long time;
			String tag;
			VOUsuario usuarioTempo;

			while(line!= null){

				temp = new VOTag();
				data = line;
				dataSplit = data.split(",");
				usuarioTempo = new VOUsuario();


				if(dataSplit.length == 4 && isNumber(dataSplit[0])){
					time = Long.parseLong(dataSplit[3]);
					tag = dataSplit[2];
					temp.setTag(tag);
					temp.setTimestamp(time);
					temp.setIdUsuario(Long.parseLong(dataSplit[0]));
					temp.setIdPelicula(Long.parseLong(dataSplit[1]));
					tags.agregarElementoFinal(temp);

					usuarioTempo.setIdUsuario(Long.parseLong(dataSplit[0]));
					usuarioTempo.setTag(tag);
					usuarioTempo.setPrimerTimestamp(time);
					usuarioTempo.setIdTagToPelicula(Long.parseLong(dataSplit[1]));
					usuariosTags.agregarElementoFinal(usuarioTempo);
				}
				line = br.readLine();

			}
			if(tags.darNumeroElementos() > 0)
				cargado = true;

			br.close();

		}
		catch(IOException e){
			e.printStackTrace();
			cargado = false;
		}

		return cargado;
	}

	@Override
	public int sizeMoviesSR() {
		return arbolPeliculas.size();
	}

	@Override
	public int sizeUsersSR() {
		return arbolUsuarios.size();
	}

	@Override
	public int sizeTagsSR() {
		return auxTag.length;
	}

	@Override
	public void registrarSolicitudRecomendacion(Integer idUsuario, String ruta) {

		if(ruta == null){

			if(arbolIdUsuariosSolicitudes.contains(idUsuario)){System.out.println("Ese usuario ya tiene registrada una solicitud"); return;}

			if(!arbolUsuarios.contains(new Long(idUsuario))){System.out.println("No existe ningun usuario con ese id"); return;}

			arbolIdUsuariosSolicitudes.put(idUsuario, idUsuario);
			colaSolicitudes.insertSolicitud(arbolUsuarios.get(new Long(idUsuario)), SOLICITUD_ID);

		}else{

			if(arbolRutasJson.contains(ruta)){System.out.println("Los datos provenientes de esa ruta ya han sido leidos"); return;}

			VOUsuario aux = new VOUsuario();

			CargadorSolicitudJSON cargador = new CargadorSolicitudJSON();

			Long idUsuario2 = cargador.cargarSolicitud(arbolUsuarioIdUsuarioRating,ruta, aux, usuariosRating,listaRatings);

			if(idUsuario2 == Long.MAX_VALUE){return;}

			System.out.println("El id asignado al usuario del Json es:" + idUsuario2);

			aux.setIdUsuario(idUsuario2.longValue());

			colaSolicitudes.insertSolicitud(aux, SOLICITUD_JSON);
			pasarListasArreglo();
			arbolRutasJson.put(ruta, ruta);
		}
	}


	@Override
	public void generarRespuestasRecomendaciones() {

		if(colaSolicitudes.isEmpty()){System.out.println("Es imposible ejecutar este m�todo si no se han hecho solicitudes de recomendaci�n.");return;}

		VOUsuario agregar = new VOUsuario();
		ListaEncadenada<ListaEncadenada<VOPrediccion>> listaPredicciones = new ListaEncadenada<>();


		for( int i = 0; i < 10 && !colaSolicitudes.isEmpty(); i++ )
		{
			agregar = colaSolicitudes.delMaxSolicitud();
			listaPredicciones.agregarElementoFinal(obtener5MejoresPredicciones((agregar.getIdUsuario())));
		}

		generarJsonReporte.generarReporte(listaPredicciones);

	}

	private void calcularSimilitud(Long idUsuario)
	{

	}


	@Override
	public ILista<VOPelicula> peliculasGenero(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		// TODO requerimiento 3
		return tablaRequerimiento3.getPeliculasRango(genero, fechaInicial, fechaFinal);
	}

	@Override
	public void agregarRatingConError(int idUsuario, int idPelicula, Double rating) {
		calcularPrediccionesUsuario(idUsuario);
		VORating buscar = new VORating();
		VORating buscado= new VORating();
		buscar.setIdPelicula(idPelicula);
		ComparatorByIDPelicula comp2 = buscar.darComparadorIDPElcila();
		buscado = listaRatings.buscarLlave(buscar, comp2);
		double error = 0;

		buscado.setRating(rating);
		error =  Math.abs(rating - buscado.getPrediccion());
		buscado.setError(error);
		System.out.println("Error: " + error );
		pasarListasArreglo();
	}

	public ILista<VOUsuarioPelicula> informacionInteraccionUsuario(Long idUsuario) {
		ListaEncadenada<VOUsuarioPelicula> peliculasUsuario = new ListaEncadenada<>();
		ListaEncadenada<VOTag> tagsAsociados;
		VORating buscar = new VORating();
		VORating buscado = new VORating();
		VOUsuarioPelicula agregar = new VOUsuarioPelicula();
		ComparatorByIDUsuario comp = buscado.darComparadorIDUsuario();

		for(int i = 0; i < listaRatings.darNumeroElementos(); i++ )
		{
			//	System.out.println("entrox2");
			buscar.setIdUsuario(idUsuario);
			buscado = listaRatings.buscarLlave(buscar, comp);
			if( buscado != null )
			{

				agregar.setIdUsuario(idUsuario);
				agregar.setErrorRating(buscado.getError());
				agregar.setNombrepelicula(arbolPeliculas.get(buscado.getIdPelicula()).getTitulo());
				agregar.setRatingSistema(buscado.getPrediccion());
				agregar.setRatingUsuario(buscado.getRating());
				tagsAsociados = new ListaEncadenada<>();
				for( int j = 0; j < tags.darNumeroElementos(); j++ )
				{
					if( tags.darElemento(j).getIdPelicula() == buscado.getIdPelicula() && tags.darElemento(j).getIdUsuario() == buscado.getIdUsuario() )
					{
						tagsAsociados.agregarElementoFinal(tags.darElemento(j));
					}
				}
				agregar.setTags(tagsAsociados);
				peliculasUsuario.agregarElementoFinal(agregar);
			}
		}
		return peliculasUsuario;

	}

	@Override
	public void clasificarUsuariosPorSegmento() {
		// TODO requerimiento 6

		boolean cambioUsuario = false;


		QuickSortTags<VOTag> quickTagsId = new QuickSortTags<VOTag>();
		quickTagsId.sort(auxTag);

		ListaEncadenada<String> listaInconforme;
		ListaEncadenada<String> listaConforme;
		ListaEncadenada<String> listaNeutral;
		ListaEncadenada<String> listaNoClasificado;

		Long idUsuario;
		String tag;
		String categoria;

		VOTag aux;
		idUsuario = auxTag[0].getIdUsuario();
		tag = auxTag[0].getTag();
		int pos =0;
		int noClasifi =0;
		int confor =0;
		int neutral = 0;
		int inconfor =0;
		boolean terminado = false;


		for(int j =0; j < auxTag.length && !terminado; j++){

			cambioUsuario = false;
			listaNoClasificado = new ListaEncadenada<>();
			listaNeutral = new ListaEncadenada<>();
			listaConforme = new ListaEncadenada<>();
			listaInconforme = new ListaEncadenada<>();
			if(pos == auxTag.length)terminado = true;

			for(int i =pos; i < auxTag.length && !cambioUsuario; i++){


				if(idUsuario == auxTag[i].getIdUsuario()){	

					tag = auxTag[i].getTag();
					categoria = arbolCategorias.get(tag);
					if(categoria == null){listaNoClasificado.agregarElementoFinal(NO_CLASIFICADO);}
					else{
						if(categoria.equals(NO_CLASIFICADO)){listaNoClasificado.agregarElementoFinal(categoria);}
						else if(categoria.equals(CONFORME)){listaConforme.agregarElementoFinal(categoria);}
						else if(categoria.equals(NEUTRAL)){listaNeutral.agregarElementoFinal(categoria);}
						else listaInconforme.agregarElementoFinal(categoria);
					}
				}
				else{
					pos =i;
					cambioUsuario = true;

					noClasifi = listaNoClasificado.darNumeroElementos();
					confor = listaConforme.darNumeroElementos();
					neutral = listaNeutral.darNumeroElementos();
					inconfor = listaInconforme.darNumeroElementos();

					if(noClasifi >= confor && noClasifi >= neutral && noClasifi >= inconfor){diccionario.put(NO_CLASIFICADO, idUsuario);}
					else if(confor >= noClasifi && confor >= neutral && confor >= inconfor){diccionario.put(CONFORME, idUsuario);}
					else if(neutral >= noClasifi && neutral >= confor && neutral >= inconfor){diccionario.put(NEUTRAL,idUsuario);}
					else{diccionario.put(INCONFORME, idUsuario);}

					idUsuario = auxTag[pos].getIdUsuario();

				}

			}
		}


		ArbolBinarioInfo<Long,Long> arbolUsuarios2 = new ArbolBinarioInfo<>();

		for(int i=0; i < auxRating.length; i++){
			idUsuario = auxRating[i].getIdUsuario();
			if(!arbolUsuarioIdUsuarioTags.contains(idUsuario)) arbolUsuarios2.put(idUsuario, idUsuario);
		}



		ListaEncadenada<Long> listaUsuarios = new ListaEncadenada<>();

		arbolUsuarios2.inOrderLista(listaUsuarios);

		Long id;
		listaUsuarios.cambiarActualPrimero();
		for(int i =0; i < listaUsuarios.darNumeroElementos();i++){
			id = listaUsuarios.darElementoPosicionActual();
			diccionario.put(NO_CLASIFICADO, id);
			listaUsuarios.avanzarSiguientePosicion();
		}

		conformes = diccionario.get(CONFORME);
		inConformes = diccionario.get(INCONFORME);
		neutrales = diccionario.get(NEUTRAL);
		noClasificados = diccionario.get(NO_CLASIFICADO);
		Long idUsuarioConforme;

		if(conformes != null){
			System.out.println("Los usuarios conformes son: ");


			conformes.cambiarActualPrimero();

			for(int i =0; i < conformes.darNumeroElementos(); i++ ){
				idUsuarioConforme = conformes.darElementoPosicionActual();
				System.out.println("IdUsuario: " + idUsuarioConforme);
				conformes.avanzarSiguientePosicion();
			}
		}else{System.out.println("No hay usuarios conformes");}


		if(inConformes != null){

			System.out.println("Los usuarios inconformes son: ");

			inConformes.cambiarActualPrimero();

			for(int i =0; i < inConformes.darNumeroElementos(); i++){
				idUsuarioConforme = inConformes.darElementoPosicionActual();
				System.out.println("IdUsuario: " + idUsuarioConforme);
				inConformes.avanzarSiguientePosicion();
			}

		}else{System.out.println("No hay usuarios inconformes");}


		if(neutrales != null){

			System.out.println("Los usuarios neutrales son: ");

			neutrales.cambiarActualPrimero();

			for(int i =0; i < neutrales.darNumeroElementos(); i++ ){
				idUsuarioConforme = neutrales.darElementoPosicionActual();
				System.out.println("IdUsuario: " + idUsuarioConforme);
				neutrales.avanzarSiguientePosicion();
			}

		}else{System.out.println("No hay usuarios neutrales");}



		if(noClasificados != null){

			System.out.println("Los usuarios no clasificados son: ");

			noClasificados.cambiarActualPrimero();

			for(int i =0; i < noClasificados.darNumeroElementos(); i++){
				idUsuarioConforme = noClasificados.darElementoPosicionActual();
				System.out.println("IdUsuario: " + idUsuarioConforme);
				noClasificados.avanzarSiguientePosicion();
			}

		}else{System.out.println("No hay usuarios no Clasificados");}

	}

	@Override
	public void ordenarPeliculasPorAnho() {
		// TODO Arbol Requerimiento 7

		arbolRequerimiento7 = new ArbolBinarioHash<Integer, VOPeliculaUsuarios>();
		Long idPeliculaTag;
		Long idPeliculaPeli;
		Long idPeliculaUser;
		BinaryTreeHashTableTags<String,VOUsuario> tablaUsuarios;
		VOPeliculaUsuarios peliculaUserAux;
		boolean peliculaConTag;
		boolean usuariosTerminados;
		int pos =0;
		for(int i =0; i < auxPeli.length; i++){
			peliculaUserAux = null;
			idPeliculaPeli = auxPeli[i].getIdPelicula();
			peliculaConTag = false;

			for(int j =pos; j < auxTag.length && !peliculaConTag; j++){
				idPeliculaTag = auxTag[j].getIdPelicula();

				if(idPeliculaTag.longValue() > idPeliculaPeli.longValue()){peliculaConTag = true; pos = j;}

				if(idPeliculaTag.longValue() == idPeliculaPeli.longValue()){

					peliculaUserAux = new VOPeliculaUsuarios();
					peliculaUserAux.setPelicula(auxPeli[i]);
					tablaUsuarios = new BinaryTreeHashTableTags<String,VOUsuario>(4);
					usuariosTerminados = false;

					for(int h =j; h < auxUsuariosTags.length && !usuariosTerminados; h++){
						idPeliculaUser = auxUsuariosTags[h].getIdTagToPelicula();

						if(idPeliculaUser.longValue() > idPeliculaPeli.longValue()){usuariosTerminados = true; pos = h;}

						if(idPeliculaUser.longValue() == idPeliculaTag.longValue()){
							tablaUsuarios.put(auxUsuariosTags[h].getTag(), auxUsuariosTags[h]);
						}

					}

					peliculaUserAux.setUsuarios(tablaUsuarios);
					if(peliculaUserAux != null){
						Calendar calendar1 = Calendar.getInstance();
						calendar1.setTime(peliculaUserAux.getPelicula().getFechaLanzamiento());
						Integer year = calendar1.get(Calendar.YEAR);
						arbolRequerimiento7.put(year,peliculaUserAux);
						peliculaConTag = true;
					}
				}

			}
		}

		arbolRequerimiento7.inOrder();
	}	

	@Override
	public VOReporteSegmento generarReporteSegmento(String segmento) {
		ListaEncadenada<Long> respuesta = new ListaEncadenada<>();
		double sumatoriaErrores = 0;
		ListaEncadenada<VOUsuarioPelicula> usuarios = new ListaEncadenada<>();
		int numeroErrores = 0;
		double errorPromedio = 0;
		VOReporteSegmento reporte = new VOReporteSegmento();
		if( segmento.equals(CONFORME))
		{
			respuesta = conformes;
		}
		else if( segmento.equals(INCONFORME))
		{
			respuesta = inConformes;
		}
		else if( segmento.equals(NEUTRAL))
		{
			respuesta = neutrales;
		}
		else if( segmento.equals(NO_CLASIFICADO))
		{
			respuesta = noClasificados;
		}
//
//		ListaEncadenada<VOTag> tagsPeli;
//		String categoria = "";
//		int noClas = 0;
//		int conforme = 0;
//		int neutral = 0;
//		int inconforme = 0;
		respuesta.cambiarActualPrimero();
		for( int i = 0; i < respuesta.darNumeroElementos(); i++ )
		{
			usuarios = (ListaEncadenada<VOUsuarioPelicula>) informacionInteraccionUsuario(respuesta.darElementoPosicionActual());
			usuarios.cambiarActualPrimero();
			for(int j = 0; j < usuarios.darNumeroElementos(); j++)
			{
				sumatoriaErrores += usuarios.darElementoPosicionActual().getErrorRating();
				numeroErrores++;
				
//				tagsPeli = new ListaEncadenada<>();
//				tagsPeli = (ListaEncadenada<VOTag>) usuarios.darElementoPosicionActual().getTags();
//				tagsPeli.cambiarActualPrimero();
//				noClas = 0;
//				conforme= 0;
//				inconforme= 0;
//				neutral = 0;
//				for( int k = 0; k < tagsPeli.darNumeroElementos(); k++ )
//				{
//					categoria= arbolCategorias.get(tagsPeli.darElementoPosicionActual().getTag());
//					
//					
//					if(categoria == null){noClas++;}
//					else{
//						if(categoria.equals(NO_CLASIFICADO)){noClas++;}
//						else if(categoria.equals(CONFORME)){conforme++;}
//						else if(categoria.equals(NEUTRAL)){neutral++;}
//						else inconforme++;
//					}
//					tagsPeli.avanzarSiguientePosicion();
//					
//				}
//				if()
				usuarios.avanzarSiguientePosicion();
			}
			respuesta.avanzarSiguientePosicion();
		}

		if( numeroErrores != 0 )
		{
			errorPromedio = sumatoriaErrores/numeroErrores;
		}
		else
		{
			errorPromedio = 0;
		}
		reporte.setErrorPromedio(errorPromedio);
		
		
		

		return reporte;
	}

	@Override
	public ILista<VOPelicula> peliculasGeneroPorFechaLanzamiento(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		// TODO Auto-generated method stub
		return tablaRequerimiento3.getPeliculasRango(genero, fechaInicial, fechaFinal);
	}

	@Override
	public ILista<VOPelicula> peliculasMayorPrioridad(int n) {
		// TODO requerimiento 10

		ListaEncadenada<VOPelicula> listaRetornar = new ListaEncadenada<VOPelicula>();

		for(int i =0; i < n; i++){
			listaRetornar.agregarElementoFinal(maxHeapReq10.delMax().getPelicula());
		}
		return listaRetornar;
	}

	private void cargarMaxHeap(){

		VOPeliculaPrioridad aux;
		ListaEncadenada<VOPeliculaPrioridad> listaAux = new  ListaEncadenada<VOPeliculaPrioridad>();

		double prioridad;
		for(int i =0; i < auxPeli.length; i++){
			aux = new VOPeliculaPrioridad();
			aux.setPelicula(auxPeli[i]);
			prioridad = auxPeli[i].getRatingIMDB()* auxPeli[i].getPromedioAnualDeVotos();
			auxPeli[i].setPrioridad(prioridad);
			aux.setPrioridad(prioridad);
			listaAux.agregarElementoFinal(aux);
		}

		VOPeliculaPrioridad[] arregloPrioridad = new VOPeliculaPrioridad[listaAux.darNumeroElementos()];

		listaAux.cambiarActualPrimero();
		for(int i =0; i < arregloPrioridad.length; i++){
			arregloPrioridad[i] = listaAux.darElementoPosicionActual();
			listaAux.avanzarSiguientePosicion();
		}

		for(int i =0; i < arregloPrioridad.length; i++){
			maxHeapReq10.insert(arregloPrioridad[i]);
		}
	}

	@Override
	public ILista<VOPelicula> consultarPeliculasFiltros(Integer anho, String pais, VOGeneroPelicula genero) {
		ordenarPeliculasPorAnho();
		ListaEncadenada<VOPelicula> consulta = new ListaEncadenada<>();
		if( anho != null && pais != null && genero != null )
		{
			BinaryTreeHashList<String,VOPeliculaUsuarios> buscado1 = new BinaryTreeHashList<>();
			buscado1 = arbolRequerimiento7.get(anho);
			ArbolBinarioList<Double,VOPeliculaUsuarios> buscado2 = new ArbolBinarioList<>();
			buscado2 = buscado1.get(genero.getNombre());
			for( Double s: buscado2.keys() )
			{
				ListaEncadenada<VOPeliculaUsuarios> lista = buscado2.get(s);
				for( int i = 0; i < lista.darNumeroElementos(); i++ )
				{
					if( (lista.darElemento(i).getPelicula().getPais()).equals(pais) )
					{
							consulta.agregarElementoFinal(lista.darElemento(i).getPelicula());
					}
				}
			}
		}
		else if( anho != null && pais != null && genero == null  )
		{
			BinaryTreeHashList<String,VOPeliculaUsuarios> buscado1 = new BinaryTreeHashList<>();
			buscado1 = arbolRequerimiento7.get(anho);
			for(String l : buscado1.keys())
			{
				ArbolBinarioList<Double,VOPeliculaUsuarios> arbol = buscado1.get(l);
				for( Double s: arbol.keys() )
				{
					ListaEncadenada<VOPeliculaUsuarios> lista = arbol.get(s);
					for( int i = 0; i < lista.darNumeroElementos(); i++ )
					{
						if( (lista.darElemento(i).getPelicula().getPais()).equals(pais) )
						{
							consulta.agregarElementoFinal(lista.darElemento(i).getPelicula());
						}
					}
				}
			}
		}
		else if( anho != null && pais == null && genero != null)
		{
			BinaryTreeHashList<String,VOPeliculaUsuarios> buscado1 = new BinaryTreeHashList<>();
			buscado1 = arbolRequerimiento7.get(anho);
			ArbolBinarioList<Double,VOPeliculaUsuarios> buscado2 = new ArbolBinarioList<>();
			buscado2 = buscado1.get(genero.getNombre());
			for( Double s: buscado2.keys() )
			{
				ListaEncadenada<VOPeliculaUsuarios> lista = buscado2.get(s);
				for( int i = 0; i < lista.darNumeroElementos(); i++ )
				{
					consulta.agregarElementoFinal(lista.darElemento(i).getPelicula());
				}
			}
		}
		else if( anho == null && pais != null && genero != null )
		{
			for( Integer k: arbolRequerimiento7.keys() )
			{
				BinaryTreeHashList<String,VOPeliculaUsuarios> buscado1 = new BinaryTreeHashList<>();
				buscado1 = arbolRequerimiento7.get(k);
				ArbolBinarioList<Double,VOPeliculaUsuarios> buscado2 = new ArbolBinarioList<>();
				buscado2 = buscado1.get(genero.getNombre());
				for( Double s: buscado2.keys() )
				{
					ListaEncadenada<VOPeliculaUsuarios> lista = buscado2.get(s);
					for( int i = 0; i < lista.darNumeroElementos(); i++ )
					{
						if( (lista.darElemento(i).getPelicula().getPais()).equals(pais) )
						{
							consulta.agregarElementoFinal(lista.darElemento(i).getPelicula());
						}
					}
				}
			}
		}
		else if( anho == null && pais != null && genero == null )
		{
			for( Integer k: arbolRequerimiento7.keys() )
			{
				BinaryTreeHashList<String,VOPeliculaUsuarios> buscado1 = new BinaryTreeHashList<>();
				buscado1 = arbolRequerimiento7.get(k);
				for(String l : buscado1.keys())
				{
					ArbolBinarioList<Double,VOPeliculaUsuarios> arbol = buscado1.get(l);
					for( Double s: arbol.keys() )
					{
						ListaEncadenada<VOPeliculaUsuarios> lista = arbol.get(s);
						for( int i = 0; i < lista.darNumeroElementos(); i++ )
						{
							if( (lista.darElemento(i).getPelicula().getPais()).equals(pais) )
							{
								consulta.agregarElementoFinal(lista.darElemento(i).getPelicula());
							}
						}
					}
				}
			}
		}
		else if( anho == null && pais == null && genero != null)
		{
			for( Integer k: arbolRequerimiento7.keys() )
			{
				BinaryTreeHashList<String,VOPeliculaUsuarios> buscado1 = new BinaryTreeHashList<>();
				buscado1 = arbolRequerimiento7.get(k);
				ArbolBinarioList<Double,VOPeliculaUsuarios> buscado2 = new ArbolBinarioList<>();
				buscado2 = buscado1.get(genero.getNombre());
				for( Double s: buscado2.keys() )
				{
					ListaEncadenada<VOPeliculaUsuarios> lista = buscado2.get(s);
					for( int i = 0; i < lista.darNumeroElementos(); i++ )
					{
						consulta.agregarElementoFinal(lista.darElemento(i).getPelicula());
					}
				}
			}
		}
		else if( anho != null && pais == null && genero == null )
		{
			BinaryTreeHashList<String,VOPeliculaUsuarios> buscado1 = new BinaryTreeHashList<>();
			buscado1 = arbolRequerimiento7.get(anho);
			for(String l : buscado1.keys())
			{
				ArbolBinarioList<Double,VOPeliculaUsuarios> arbol = buscado1.get(l);
				for( Double s: arbol.keys() )
				{
					ListaEncadenada<VOPeliculaUsuarios> lista = arbol.get(s);
					for( int i = 0; i < lista.darNumeroElementos(); i++ )
					{
						consulta.agregarElementoFinal(lista.darElemento(i).getPelicula());
					}
				}
			}
		}

		VOPelicula[] aux = new VOPelicula[consulta.darNumeroElementos()];
		
		consulta.cambiarActualPrimero();
		for(int i =0; i< aux.length; i++){
			aux[i] = consulta.darElementoPosicionActual();
			consulta.avanzarSiguientePosicion();
		}
		
		QuickSortPeliculasReq11<VOPelicula> quickPelicula11 = new QuickSortPeliculasReq11<>();
		quickPelicula11.sort(auxPeli);
		
		consulta = new ListaEncadenada<>();
		
		for(int i =0; i < aux.length; i++){
			consulta.agregarElementoFinal(aux[i]);
		}
		
		
//		VOPelicula a = new VOPelicula();
//		ComparatorByAgno comp1 = a.darComparadorAgno();
//		ComparatorAlfabetico comp2 = a.darComparadorAlfabetico();
//		ComparatorRating comp3 = a.darComparadorRating();
//		consulta.sort3Criterios(consulta, comp1, comp2, comp3, true);
		return consulta;
	}


	public void calcularSimilitudesPeliculas( ) throws FileNotFoundException
	{
		Calendar cal = Calendar.getInstance();
		Long n = cal.getTimeInMillis();
		PrintWriter pw = new PrintWriter(new File("similitudes.csv"));
		StringBuilder sb = new StringBuilder();
		sb.append("idPelicula 1");
		sb.append(',');
		sb.append("idPelicula 2");
		sb.append(',');
		sb.append("similitud");
		sb.append('\n');

		EncadenamientoSeparadoTH<Long, EncadenamientoSeparadoTH<Long, Double>> infoProcesar = new EncadenamientoSeparadoTH<>(9127);
		ListaEncadenada<VORating> pelicula1;
		ListaEncadenada<VORating> pelicula2;
		EncadenamientoSeparadoTH<Long, Double> usuarios;

		for( int i = 0; i < auxPeli.length; i++ )
		{
			usuarios = new EncadenamientoSeparadoTH<>(100);
			infoProcesar.insertar(auxPeli[i].getIdPelicula(), usuarios);
		}

		for( int i = 0; i < auxRating.length ; i++)
		{
			usuarios = infoProcesar.darValor(auxRating[i].getIdPelicula());


			if( usuarios != null )
			{
				usuarios.insertar(auxRating[i].getIdUsuario(), auxRating[i].getRating());
			}

		}

		double numerador = 0;
		double denominador = 0;
		double factor1 = 0;
		double factor2 = 0;
		double similitud =0;
		VORating rating1;
		VORating rating2;
		Long id1 = (long)0;
		Long id2= (long)0;

		for( int i = 0; i < auxPeli.length-1; i ++ )
		{	

			id1 = auxPeli[i].getIdPelicula();
			for( int j = i+1; j < auxPeli.length; j++ )
			{				
				pelicula2 = new  ListaEncadenada<>();
				pelicula1 = new  ListaEncadenada<>();
				id2 = auxPeli[j].getIdPelicula();

				for (Long s : infoProcesar.darValor(id1).keys())
				{

					if( infoProcesar.darValor(id2).tieneLlave( s ) )
					{
						rating1 = new VORating();
						rating2 = new VORating();

						rating1.setRating(infoProcesar.darValor(id1).darValor(s));
						pelicula1.agregarElementoFinal(rating1);
						rating2.setRating(infoProcesar.darValor(id2).darValor(s));
						pelicula2.agregarElementoFinal(rating2);
					}

				}

				if(!pelicula1.isEmpty()){
					numerador = 0;
					denominador = 0;
					factor1 = 0;
					factor2 = 0;
					similitud = 0;
					if( pelicula1.darNumeroElementos() >= 3 && pelicula2.darNumeroElementos() >= 3 )
					{
						for( int k = 0; k < pelicula1.darNumeroElementos()-1; k++ )
						{
							numerador += (pelicula1.darElemento(k).getRating() * pelicula2.darElemento(k).getRating());
							factor1 += Math.pow(pelicula1.darElemento(k).getRating(), 2);
							factor2 += Math.pow(pelicula2.darElemento(k).getRating(), 2);
						}
					}

					denominador = Math.sqrt(factor1)*Math.sqrt(factor2);
					if( denominador == 0 )
					{
						similitud = 0;
					}
					else{
						similitud = numerador/denominador; 
					}

					sb.append(Long.toString(id1));
					sb.append(',');
					sb.append(Long.toString(id2));
					sb.append(',');
					sb.append(Double.toString(similitud));
					sb.append('\n');
					sb.append(Long.toString(id2));
					sb.append(',');
					sb.append(Long.toString(id1));
					sb.append(',');
					sb.append(Double.toString(similitud));
					sb.append('\n');
					System.out.println("Similitud: " + similitud);
					//contador++;


				}
			}

		}

		pw.write(sb.toString());
		pw.close();
		System.out.println("done!");

	}

	public ListaEncadenada<VOPrediccion> calcularPrediccionesUsuario( long idUsuario )
	{
		ListaEncadenada<VOUsuarioPelicula> peliculasUsuario =  new ListaEncadenada<>();
		ListaEncadenada<VOPrediccion> prediccionesFinal = new ListaEncadenada<>(); //Key idPelicula
		peliculasUsuario = (ListaEncadenada<VOUsuarioPelicula>) informacionInteraccionUsuario(idUsuario);
		ListaEncadenada<VOPrediccion> predicciones = new ListaEncadenada<VOPrediccion>();
		System.out.println(informacionInteraccionUsuario(idUsuario).darNumeroElementos());
		double numerador = 0;
		double denominador = 0;
		double prediccion = 0;
		VOPrediccion prediccionVO = new VOPrediccion();
		VORating agregar = new VORating();
		VOUsuarioPelicula buscar = new VOUsuarioPelicula();
		ComparatorByIDPeliculaUP comp = buscar.darComparadorIDPelicula();

		for( int i = 0; i < auxPeli.length; i++ )
		{

			prediccionVO = new VOPrediccion();
			agregar = new VORating();
//			System.out.println(similitudes.darTamanio());
			if( similitudes.darValor(auxPeli[i].getIdPelicula()) != null && similitudes.darValor(peliculasUsuario.darElementoPosicionActual().getIDPelicula()) != null)
			{
				System.out.println(similitudes.darValor(auxPeli[i].getIdPelicula()));
				peliculasUsuario.cambiarActualPrimero();
				for( int j = 0; j < peliculasUsuario.darNumeroElementos(); j++ )
				{
					
					numerador += (peliculasUsuario.darElementoPosicionActual().getRatingUsuario())*(similitudes.darValor(auxPeli[i].getIdPelicula()).darValor(peliculasUsuario.darElemento(j).getIDPelicula()));
					denominador += similitudes.darValor(auxPeli[i].getIdPelicula()).darValor(peliculasUsuario.darElemento(j).getIDPelicula());

					peliculasUsuario.avanzarSiguientePosicion();
				}
			}
			
			if(denominador != 0)
			{
				prediccion = numerador/denominador;
			}
			else
			{
				prediccion = 0;
			}
			System.out.println("IdPelicula: " + auxPeli[i].getIdPelicula());
			System.out.println("Prediccion: " + prediccion);
			prediccionVO.setIDUsuario(idUsuario);
			prediccionVO.setIDPelicula(auxPeli[i].getIdPelicula());
			prediccionVO.setPrediccion(prediccion);
			predicciones.agregarElementoFinal(prediccionVO);
			agregar = new VORating();
			agregar.setIdPelicula(auxPeli[i].getIdPelicula());
			agregar.setIdUsuario(idUsuario);
			agregar.setPrediccion(prediccion);
			listaRatings.agregarElementoFinal(agregar);


		}
		
		predicciones = pasarPrediccionesLista(pasarPrediccionesArreglo(predicciones));
		
		return predicciones;
	}

	public ArbolBinarioInfo<String, String> getGeneros( )
	{
		return generos;
	}
	
	private ListaEncadenada<VOPrediccion> obtener5MejoresPredicciones( Long idUsuario )
	{
		ListaEncadenada<VOPrediccion> peliculasUsuario = calcularPrediccionesUsuario(idUsuario);
		ListaEncadenada<VOPrediccion> retornar = new ListaEncadenada<>();
		
		peliculasUsuario.cambiarActualPrimero();
		for( int i = 0; i < 5; i++ )
		{
			retornar.agregarElementoFinal(peliculasUsuario.darElementoPosicionActual());
			peliculasUsuario.avanzarSiguientePosicion();
		}
		
		return retornar;
	}

}
