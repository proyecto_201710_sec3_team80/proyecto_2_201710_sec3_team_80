package model.logic;

import java.io.File;
import java.io.FileWriter;
import java.time.Year;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import model.data_structures.ListaEncadenada;
import model.vo.VOPrediccion;

public class generarJsonReporte {

	 public static String ruta;

	public static  void generarReporte(ListaEncadenada<ListaEncadenada<VOPrediccion>> listaPredicciones){

		JsonObject response = new JsonObject();
		JsonArray response_users = new JsonArray();
		JsonPrimitive user_id;
		listaPredicciones.cambiarActualPrimero();
		JsonObject obj;
		ListaEncadenada<VOPrediccion> listaAux;
		VOPrediccion prediccionAux;
		JsonArray recomendations;
		JsonObject item;
		JsonPrimitive item_id;
		JsonPrimitive p_rating;

		for(int i =0; i < listaPredicciones.darNumeroElementos(); i++){
			listaAux = listaPredicciones.darElementoPosicionActual();

			listaAux.cambiarActualPrimero();

			obj = new JsonObject();
			prediccionAux = listaAux.darElementoPosicionActual();

			user_id = new JsonPrimitive(prediccionAux.getIDUsuario());
			obj.add("user_id",user_id);

			recomendations = new JsonArray();

			for(int j =0; j < listaAux.darNumeroElementos(); j++){
				prediccionAux = listaAux.darElementoPosicionActual();

				item = new JsonObject();
				item_id = new JsonPrimitive(prediccionAux.getIDPelicula());
				p_rating = new JsonPrimitive(prediccionAux.getPrediccion());

				item.add("item_id", item_id);
				item.add("p_rating",p_rating);

				recomendations.add(item);

				listaAux.avanzarSiguientePosicion();
			}
			obj.add("recomendations", recomendations);

			response_users.add(obj);

			listaPredicciones.avanzarSiguientePosicion();
		}

		response.add("response_users",response_users);

		try{

			Date fecha = new Date();
			Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
			calendar.setTime(fecha);

			int year = calendar.get(calendar.YEAR);
			int mes = calendar.get(calendar.MONTH);
			int day = calendar.get(calendar.DAY_OF_MONTH);
			int hour = calendar.get(calendar.HOUR_OF_DAY);
			int min = calendar.get(calendar.MINUTE);
			int sec = calendar.get(calendar.SECOND);

			ruta = "./data/Recomendaciones_<" + day +"-"+ mes +"-"+ year +"_" + hour + "_" + min + "_" + sec + ">.json" ;
			FileWriter file   = new FileWriter(ruta);
			file.write(response.toString());
			
			file.flush();
			file.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}


