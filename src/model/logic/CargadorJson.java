package model.logic;

import java.io.FileReader;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;

import model.data_structures.BinaryTreeHashTable;
import model.data_structures.ListaEncadenada;
import model.vo.VOInfoPelicula;
import model.vo.VOPelicula;



public class CargadorJson {

	private VOPelicula[] auxPeli;

	private VOPelicula pelicula;


	public boolean cargarPeliculas(VOPelicula[] auxPeli, BinaryTreeHashTable<String, VOPelicula> tablaRequerimiento3)
	{
		boolean cargado = false;
		this.auxPeli = auxPeli;
		JsonParser parser = new JsonParser();
		try
		{
			JsonElement jelement = parser.parse(new FileReader("./data/links_json.json"));
			JsonArray jArray = jelement.getAsJsonArray();
			


			String rate;
			String titulo;
			String[] actores;
			String actor;
			String director;
			String pais;
			Long movieId;
			double ratingIMDB;
			String date;
			Date fechaLanzamiento = new Date();
			int votosTotales =0;
			DateFormat format = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
			double promedioAnualVotos = 0;
			int year;
			Calendar calendar1;
			String votosString;
			
			JsonPrimitive title;
			JsonPrimitive fecha;
			JsonPrimitive actors;
			JsonPrimitive Director;
			JsonPrimitive rating;
			JsonPrimitive votos;
			JsonPrimitive country;



			for(int i = 0; i < jArray.size(); i++)
			{

				JsonObject jobject = jArray.get(i).getAsJsonObject();
				JsonPrimitive id = jobject.getAsJsonPrimitive("movieId");
				movieId = new Long(id.getAsInt());



				JsonObject imdbData = jobject.getAsJsonObject("imdbData");
				title = imdbData.getAsJsonPrimitive("Title");
				titulo =title.getAsString();


				fecha = imdbData.getAsJsonPrimitive("Released");
				
				date = fecha.getAsString();
				
				if(!date.equals("N/A")){
				fechaLanzamiento = new Date();
				fechaLanzamiento = format.parse(date);
				}
				
				calendar1 = Calendar.getInstance();
				year = calendar1.get(Calendar.YEAR);
				calendar1.setTime(fechaLanzamiento);
				year = year - calendar1.get(Calendar.YEAR);
				
				votos = imdbData.getAsJsonPrimitive("imdbVotes");
				votosString = votos.getAsString();
				if(!votosString.equals("N/A")){votosTotales = Integer.parseInt(votosString.replaceAll(",", ""));}else{votosTotales = 0;}
				promedioAnualVotos = votosTotales/(year);
				
				
				actors =imdbData.getAsJsonPrimitive("Actors");
				actor = actors.getAsString();
				actores = actor.split(",");

				Director = imdbData.getAsJsonPrimitive("Director");
				director = Director.getAsString();
				
				rating  = imdbData.getAsJsonPrimitive("imdbRating");
				rate = rating.getAsString();
				
				country = imdbData.getAsJsonPrimitive("Country");
				pais = country.getAsString().split(",")[0];
				if(rate.equals("N/A"))
				{
					ratingIMDB = 0;
				}
				else
					ratingIMDB = Double.parseDouble(rate);



				pelicula = auxPeli[i];
				pelicula.setActores(actores);
				pelicula.setDirector(director);
				pelicula.setRatingIMDB(ratingIMDB);
				pelicula.setTitulo(titulo);
				pelicula.setIdPelicula(movieId);
				pelicula.setVotosTotales(votosTotales);
				pelicula.setFechaLanzamiento(fechaLanzamiento);
				pelicula.setPromediAnualDeVotos(promedioAnualVotos);
				pelicula.setPais(pais);
				tablaRequerimiento3.put(pelicula.getGeneros().darElementoPosicionActual(), pelicula);


			}
			
			double despues = System.currentTimeMillis();

			System.out.println("Se cargaron las peliculas del archivo Json, la cantidad de peliculas cargadas es: ");
			cargado = true;
			System.out.println(jArray.size());

		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		return cargado;

	}

}
