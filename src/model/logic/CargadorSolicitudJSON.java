package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;

import model.data_structures.ArbolBinarioInfo;
import model.data_structures.ListaEncadenada;
import model.vo.VOInfoPelicula;
import model.vo.VOPelicula;
import model.vo.VORating;
import model.vo.VOUsuario;



public class CargadorSolicitudJSON {


	public Long cargarSolicitud(ArbolBinarioInfo<Long, VOUsuario> arbolUsuarioIdUsuarioRating, String rutaJson, VOUsuario aux, ListaEncadenada<VOUsuario> usuariosRating, ListaEncadenada<VORating> listaRatings)
	{
		boolean cargado = false;
		JsonParser parser = new JsonParser();

		Random ran = new Random();
		boolean noExiste = false;
		Long idUsuario = Long.MAX_VALUE;

		VORating ratingAux;
		VOUsuario usuarioAux;


		try
		{
			JsonElement jelement = parser.parse(new FileReader(rutaJson));

			Long item_id;
			double rating;

			JsonPrimitive Id_item;
			JsonPrimitive rati;

			JsonObject request = jelement.getAsJsonObject();

			JsonObject r = request.getAsJsonObject("request");

			JsonArray ratings = r.getAsJsonArray("ratings"); 

			aux.setNumRatings(ratings.size());
			aux.setNumeroTags(0);

			for(int i =0; i < arbolUsuarioIdUsuarioRating.max().longValue() && !noExiste; i++){

				idUsuario = ran.nextLong();
				if(!arbolUsuarioIdUsuarioRating.contains(idUsuario.longValue()) && idUsuario.longValue() > 0) noExiste = true;

			}

			for(int i = 0; i < ratings.size(); i++)
			{
				usuarioAux = new VOUsuario();
				usuarioAux.setIdUsuario(idUsuario);

				ratingAux = new VORating();
				JsonObject jobject = ratings.get(i).getAsJsonObject();

				Id_item = jobject.getAsJsonPrimitive("item_id");
				item_id = Id_item.getAsLong();

				rati = jobject.getAsJsonPrimitive("rating");
				rating = rati.getAsDouble();

				ratingAux.setIdPelicula(item_id);
				ratingAux.setRating(rating);

				usuarioAux.setIdPeliculaRaiting(item_id);

				listaRatings.agregarElementoFinal(ratingAux);
				usuariosRating.agregarElementoFinal(usuarioAux);

			}


			double despues = System.currentTimeMillis();

			System.out.println("Se carg� la solicitud del archivo Json");
			cargado = true;


		}
		catch(FileNotFoundException o){
			System.out.println("La ruta de archivo que se ingres� no corresponde a un archivo Json con el formato esperado");
		}
		catch(Exception e)
		{
			e.printStackTrace();

		}
		return idUsuario;

	}

}
