package view;

import model.data_structures.EncadenamientoSeparadoTH;

public class prueba 
{
	private static EncadenamientoSeparadoTH<String, Integer> th;
	
	public static void crearTabla()
	{
		th = new EncadenamientoSeparadoTH<>(6);
	}
	
	public static void probar()
	{
		th.insertar("Gato", 1);
		th.insertar("Perro", 2);
		th.insertar("Casa", 3);
		th.insertar("Carro", 4);
		th.insertar("Oso", 5);
		
		System.out.println(th.darValor("Gato"));
		System.out.println(th.darValor("Carro"));
		System.out.println(th.darValor("Perro"));
		System.out.println(th.darValor("Oso"));
		System.out.println(th.darValor("Casa"));
	}
	
	public static void main( String[] args)
	{
		crearTabla();
		probar();
	}

}
