package view;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthScrollBarUI;

import Controller.Controller;
import model.data_structures.ListaEncadenada;
import model.logic.generarJsonReporte;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VOReporteSegmento;
import model.vo.VOUsuarioPelicula;


public class ClienteReq {

	BufferedWriter escritor;
	Scanner lector;
	private DateFormat format = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);


	//TODO: Declarar objetos

	public ClienteReq(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;
	}

	public void pruebas() {
		int opcion = -1;

		//TODO: Inicializar objetos 


		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto ---------------\n");
				escritor.write("Requerimientos:\n");
				escritor.write("1: Registrar solicitud de recomendación a un usuario. (R1) \n");
				escritor.write("2: Generar archivo de respuesta a solicitud de usuarios. (R2) \n");
				escritor.write("3: Generar listado ordenado por fecha de peliculas en un género específico dentro de un rango de fechas. (R3) \n");
				escritor.write("4: Añadir un nuevo rating a una peli�?cula y guardar el error de prediccio�?n (R4) \n");
				escritor.write("5: Generar un informe con toda la informacio�?n que se tiene sobre un usuario y su interaccio�?n con las peli�?culas. (R5) \n");
				escritor.write("6: Se desea tener una lista con los identificadores de usuarios clasificados en un segmento dado.(R6) \n");
				escritor.write("7: Ordenar las peli�?culas del cata�?logo en un a�?rbol binario balanceado y ordenarlo por el año de la peli�?cula. (R7) \n");
				escritor.write("8: Realizar un reporte para describir los segmentos de los usuarios.(R8) \n");
				escritor.write("9: Buscar peli�?culas de un ge�?nero cuya fecha de lanzamiento se encuentre en un periodo de tiempo dado por di�?a /mes /año inicial y di�?a /mes /año final (R9) \n");
				escritor.write("10: Obtener las N peli�?culas de mayor prioridad en su orden. (R10) \n");
				escritor.write("11: Generar lista de peliculas de acuerdo a los 3 criterios ingresados por el usuario (R11) \n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();

				switch(opcion) {
				case 1: r1(); break;
				case 2: r2(); break;
				case 3: r3(); break;
				case 4: r4(); break;
				case 5: r5(); break;
				case 6: r6(); break;
				case 7: r7(); break;
				case 8: r8(); break;
				case 9: r9(); break;
				case 10: r10(); break;
				case 11: r11(); break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}

	private void r1() throws IOException{

		//TODO: Registrar solicitud de recomendación a un usuario. Recuerde que hay dos tipos de solicitud
		//RECUERDE: Usted debe manejar si el parametro es por usuario con ID o la ruta del JSON.

		lector = new Scanner(System.in);

		System.out.println("1. Solicitud por ID");
		System.out.println("2. Solicutud por JSON");

		String tipoSolicutid = lector.nextLine();

		Integer idUsuario = null;
		String rutaJson = null;

		if(tipoSolicutid.equals("1")){

			escritor.write("Ingrese el ID del usuario: \n");
			escritor.flush();
			String usuarioID = lector.next();
			idUsuario = new Integer(usuarioID);
			System.out.println(usuarioID);
		}
		else{

			escritor.write("Ingrese la ruta JSON: \n");
			escritor.flush();
			rutaJson = lector.next();
			System.out.println(rutaJson);
		}
		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar método registrarSolicitudRecomendacion(Integer idUsuario, String ruta) del API
		Controller.registrarSolicitudRecomendacion(idUsuario, rutaJson);

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}


	private void r2() throws IOException{

		//TODO: Se debe generar un archivo con la respuesta a las 10 solicitudes de recomendacio�?n de mayor prioridad en un archivo json con el formato indicado.
		//RECUERDE: El nombre del archivo a generar debe tener el siguiente formato Recomendaciones_<Dia-Mes-Año_Hora_min_seg>.json


		long tiempo = System.nanoTime();

		//TODO: Llamar método generarRespuestasRecomendaciones() del API
		//Se espera como resultado: ruta del archivo generado. 

		Controller.generarRespuestasRecomendaciones();

		String ruta = generarJsonReporte.ruta;
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("El archivo se creó con la ruta: " + ruta + "\n");
		escritor.write("\n");

		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r3() throws NumberFormatException, IOException
	{
		//TODO: retornar un listado ordenado por fecha de todas las peli�?culas de un ge�?nero particular dentro de un rango de fechas dado
		//RECUERDE: di�?a /mes /año inicial – di�?a /mes/ año final, usando el mismo formato de fecha que aparece en los datos de las peli�?culas

		escritor.write("Ingrese la fecha inicial: \n");
		escritor.flush();
		lector = new Scanner(System.in); 
		String fechaInicial = lector.nextLine();
		System.out.println(fechaInicial);

		escritor.write("Ingrese la fecha final: \n");
		escritor.flush();
		lector = new Scanner(System.in); 
		String fechaFinal = lector.nextLine();
		System.out.println(fechaFinal);

		escritor.write("Ingrese el genero a buscar: \n");
		escritor.flush();
		lector = new Scanner(System.in); 
		String genero = lector.nextLine();
		System.out.println(genero);


		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato de fechas y genero que usted maneja en su proyecto.
		Date fecha1 = null;
		Date fecha2 = null;
		Date fechaAux = null;
		try{fecha1 = format.parse(fechaInicial);}catch(Exception e){ e.printStackTrace();}
		try{fecha2 = format.parse(fechaFinal);}catch(Exception e){ e.printStackTrace();}

		if(fecha1 == null || fecha2 == null){System.out.println("Las fechas ingresadas son invalidas");return;}

		if(fecha1.compareTo(fecha2) > 0){
			fechaAux = fecha2;
			fecha2 = fecha1; 
			fecha1 = fechaAux;
		}
		VOGeneroPelicula generoPelicula = new VOGeneroPelicula();
		generoPelicula.setNombre(genero);

		long tiempo = System.nanoTime();

		//TODO: Llamar método peliculasGeneroPorFechas(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) del API
		//Se espera como resultado: lista VOPeliculas
		ListaEncadenada<VOPelicula> lista = (ListaEncadenada<VOPelicula>) Controller.peliculasGenero(generoPelicula, fecha1, fecha2);

		for(VOPelicula peli: lista){
			System.out.println("IdPelicula: "+ peli.getIdPelicula() + " Fecha de Lanzamiento " +peli.getFechaLanzamiento() + "Genero: " + peli.getGeneros().darElementoPosicionActual() );
		}
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r4() throws IOException
	{
		//TODO: Se desea que los usuarios puedan seguir añadiendo ratings a las peli�?culas; cada vez que un usuario añade un rating se compara el rating dado con la prediccio�?n dada por el sistema.
		//RECUERDE: El valor absoluto de la diferencia entre el rating otorgado por el usuario y la prediccio�?n del sistema es el error.
		//RECUERDE: Cada vez que se añade un rating se debe tener registro de cua�?nto fue el error para el rating otorgado.


		escritor.write("Ingrese el ID del Usuario: \n");
		escritor.flush();
		String idUsuario = lector.next();
		System.out.println(idUsuario);

		escritor.write("Ingrese el ID de la pelicula: \n");
		escritor.flush();
		String idPelicula = lector.next();
		System.out.println(idPelicula);

		escritor.write("Ingrese el nuevo Rating \n");
		escritor.flush();
		String rating = lector.next();
		System.out.println(rating);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.
		long tiempo = System.nanoTime();

		//TODO: Llamar método agregarRatingConError(int idUsuario, int idPelicula, Double rating) del API
		Controller.agregarRatingConError(Integer.parseInt(idUsuario), Integer.parseInt(idPelicula), Double.parseDouble(rating));
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r5() throws IOException{

		//TODO: generar un informe con toda la informacio�?n que se tiene sobre un usuario y su interaccio�?n con las peli�?culas.
		// RECUERDE: informacion a retornar en la lista: nombre Pelicula, rating otorgado por el usuario, rating calculado por el sistema,
		//error del rating, tags que el usuario otorgó a la película. 

		//RECUERDE: El error se calcula como el valor absoluto de la diferencia entre los ratings. 
		escritor.write("Ingrese el ID del usuario: \n");
		escritor.flush();
		String usuarioID = lector.next();
		System.out.println(usuarioID);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar método informacionInteraccionUsuario(int idUsuario) del API
		//Se espera como resultado: lista VOUsuarioPelicula
		ListaEncadenada<VOUsuarioPelicula> listaUP = Controller.informacionInteraccionUsuario(Long.parseLong(usuarioID));
		listaUP.cambiarActualPrimero();
		VOUsuarioPelicula aux;
		System.out.println(listaUP);
		for(int i =0; i < listaUP.darNumeroElementos(); i++){
			aux = listaUP.darElementoPosicionActual();
			System.out.println("Titulo: " + aux.getNombrepelicula() );
			System.out.println("Rating otorgado por el usuario: " + aux.getRatingUsuario());
			System.out.println("Rating prediccion: " + aux.getRatingSistema());
			System.out.println("Error: " + aux.getErrorRating());
			System.out.println("Tags: ");
			aux.getTags().cambiarActualPrimero();
			for( int j = 0; j < aux.getTags().darNumeroElementos(); j++ )
			{
				System.out.println(aux.getTags().darElementoPosicionActual().getTag());
				aux.getTags().avanzarSiguientePosicion();
			}
			listaUP.avanzarSiguientePosicion();
		}
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r6() throws IOException{

		//TODO:Se desea tener una lista con los identificadores de usuarios clasificados en un segmento dado.
		//RECUERDE: 4 tipos o segmentos diferentes: Inconformes, Conformes, Neutrales y No Clasificado.


		long tiempo = System.nanoTime();
		//TODO: Llamar método clasificarUsuariosPorSegmento() del API
		
		Controller.clasificarUsuariosPorSegmento();
		
		tiempo = System.nanoTime() - tiempo;
		
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r7() throws IOException{

		//TODO:ordenar las peli�?culas del cata�?logo en un a�?rbol binario balanceado y ordenado por el año de la peli�?cula.

		//RECUERDE:Dentro de cada nodo del a�?rbol, se tiene una lista, en la que en cada nodo se tiene una peli�?cula y 
		//una tabla de hash con los usuarios que han asignado un tag especi�?fico a dicha peli�?cula.



		long tiempo = System.nanoTime();

		Controller.ordenarPeliculasPorAnho();

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r8() throws IOException{

		//TODO:  Dado un segmento se quiere saber lo siguiente: el error promedio, sus 5 ge�?neros con ma�?s cantidad de ratings y sus 5 ge�?neros con mejor rating promedio.
		//RECUERDE: Error promedio = suma de errores sobre sus ratings dividido la cantidad de ratings con error asociado. 

		escritor.write("Ingrese el segmento: \n");
		escritor.flush();
		String segmento = lector.next();
		System.out.println(segmento);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar método generarReporteSegmento(String segmento) del API 
		//Se espera como resultado: VOReporteSegmento 
		VOReporteSegmento reporte = Controller.generarReporteSegmento(segmento);
		System.out.println("Segmento: " + segmento);
		System.out.println("Error promedio: " + reporte.getErrorPromedio());

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r9() throws NumberFormatException, IOException
	{
		escritor.write("Ingrese la fecha inicial: \n");
		escritor.flush();
		lector = new Scanner(System.in); 
		String fechaInicial = lector.nextLine();
		System.out.println(fechaInicial);

		escritor.write("Ingrese la fecha final: \n");
		escritor.flush();
		lector = new Scanner(System.in); 
		String fechaFinal = lector.nextLine();
		System.out.println(fechaFinal);

		escritor.write("Ingrese el genero a buscar: \n");
		escritor.flush();
		lector = new Scanner(System.in); 
		String genero = lector.nextLine();
		System.out.println(genero);


		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato de fechas y genero que usted maneja en su proyecto.
		Date fecha1 = null;
		Date fecha2 = null;
		Date fechaAux = null;
		try{fecha1 = format.parse(fechaInicial);}catch(Exception e){ e.printStackTrace();}
		try{fecha2 = format.parse(fechaFinal);}catch(Exception e){ e.printStackTrace();}

		if(fecha1 == null || fecha2 == null){System.out.println("Las fechas ingresadas son invalidas");return;}

		if(fecha1.compareTo(fecha2) > 0){
			fechaAux = fecha2;
			fecha2 = fecha1; 
			fecha1 = fechaAux;
		}
		VOGeneroPelicula generoPelicula = new VOGeneroPelicula();
		generoPelicula.setNombre(genero);

		long tiempo = System.nanoTime();

		//TODO: Llamar método peliculasGeneroPorFechas(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) del API
		//Se espera como resultado: lista VOPeliculas
		ListaEncadenada<VOPelicula> lista = (ListaEncadenada<VOPelicula>) Controller.peliculasGenero(generoPelicula, fecha1, fecha2);

		for(VOPelicula peli: lista){
			System.out.println("IdPelicula: "+ peli.getIdPelicula() + " Fecha de Lanzamiento " +peli.getFechaLanzamiento() + " Genero: " + peli.getGeneros().darElementoPosicionActual() );
		}
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}


	private void r10() throws IOException{

		//TODO: A partir del Heap se quiere obtener las N peli�?culas de mayor prioridad en su orden, donde el valor N es dado por el usuario.
		//RECUERDE:  Para cada peli�?cula resultante, hay que mostrar su ti�?tulo, año, votos totales, promedio anual votos y prioridad de clasificacio�?n. 

		escritor.write("Ingrese el N a buscar en el SR.\n");
		escritor.flush();
		int peliculas = lector.nextInt();
		System.out.println(peliculas);

		//RECUERDE: Utilice la variable previamente declarada y adaptela al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar método peliculasMayorPrioridad(int n) del API 
		//Se espera como resultado: lista VOPelicula		
		ListaEncadenada<VOPelicula> lista = (ListaEncadenada<VOPelicula>)Controller.peliculasMayorPrioridad(peliculas);
		lista.cambiarActualPrimero();
		VOPelicula aux;
		for(int i =0; i < lista.darNumeroElementos(); i++){
			aux = lista.darElementoPosicionActual();
			System.out.println("Titulo: " + aux.getTitulo() + " A�o: " + aux.getFechaLanzamiento().getYear() + " Votos totales: " + aux.getVotosTotales() + " promedio anual votos: " + aux.getPromedioAnualDeVotos() + " prioridad: " + aux.getPrioridad());
			lista.avanzarSiguientePosicion();
		}
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r11() throws IOException{

		//TODO: Las peli�?culas resultantes deben obtenerse en el siguiente orden: Inicialmente ordenadas por año. 
		//Para las peli�?culas del mismo año, deben ordenarse por pai�?s (alfabe�?ticamente). 
		//Para peli�?culas del mismo pai�?s deben ordenarse por ge�?nero. Para peli�?culas del mismo ge�?nero deben ordenarse por rating IMBD. 
		//Tenga en cuenta que una peli�?cula puede tener asociado mu�?ltiples pai�?ses y mu�?ltiples ge�?neros.

		//RECUERDE: los criterios pueden ser dados en cualquier orden para el filtro. 
		escritor.write("Ingrese criterio1: \n");
		escritor.flush();
		String criterio1 = lector.next();
		System.out.println(criterio1);

		escritor.write("Ingrese criterio2: \n");
		escritor.flush();
		String criterio2 = lector.next();
		System.out.println(criterio2);

		escritor.write("Ingrese el criterio3: \n");
		escritor.flush();
		String criterio3 = lector.next();
		System.out.println(criterio3);

		//RECUERDE: Utilice las variables previamente declaradas y adaptelas al formato que usted maneja en su proyecto.

		Integer anho = 2010;
		String pais = "USA";
		VOGeneroPelicula genero = new VOGeneroPelicula();
		genero.setNombre("Drama");
		try
		{
			anho = Integer.parseInt(criterio1);
			if( Controller.getGeneros().get(criterio2) != null)
			{
				pais = criterio3;
				genero.setNombre(criterio2);
			}
			else if( Controller.getGeneros().get(criterio3) != null )
			{
				pais = criterio2;
				genero.setNombre(criterio3);
			}
		}
		catch( Exception  e)
		{
			try
			{
				anho = Integer.parseInt(criterio2);
				if( Controller.getGeneros().get(criterio1) != null)
				{
					pais = criterio3;
					genero.setNombre(criterio1);
				}
				else if( Controller.getGeneros().get(criterio3) != null )
				{
					pais = criterio1;
					genero.setNombre(criterio3);
				}
			}
			catch( Exception g )
			{
				try
				{
					anho = Integer.parseInt(criterio3);
					if( Controller.getGeneros().get(criterio2) != null)
					{
						pais = criterio1;
						genero.setNombre(criterio2);
					}
					else if( Controller.getGeneros().get(criterio1) != null )
					{
						pais = criterio2;
						genero.setNombre(criterio1);
					}
				}
				catch( Exception h )
				{
					System.out.println(e.getMessage());;
				}
			}
		}
		long tiempo = System.nanoTime();
		
		ListaEncadenada<VOPelicula> respuestaFiltro = new ListaEncadenada<>();
		respuestaFiltro = (ListaEncadenada<VOPelicula>) Controller.consultarPeliculasFiltros(anho, pais, genero);
		VOPelicula aux;
		for(int i =0; i < respuestaFiltro.darNumeroElementos(); i++){
			aux = respuestaFiltro.darElementoPosicionActual();
			System.out.println("Titulo: " + aux.getTitulo() );
			System.out.println(" A�o: " + aux.getFechaLanzamiento().getYear());
			System.out.println("Director: " + aux.getDirector());
			respuestaFiltro.avanzarSiguientePosicion();
		}
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}







}
